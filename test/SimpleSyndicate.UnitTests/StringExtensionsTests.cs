﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using Xunit;

namespace SimpleSyndicate.Tests
{
    public class StringExtensionsTests
    {
        [Fact]
        public void IsEmptyReturnsFalse()
        {
            // arrange
            string original = "test";

            // assert
            Assert.False(original.IsEmpty());
        }

        [Fact]
        public void IsEmptyReturnsTrue()
        {
            // arrange
            string original = String.Empty;

            // assert
            Assert.True(original.IsEmpty());
        }

        [Fact]
        public void IsWhiteSpaceReturnsFalse()
        {
            // arrange
            string original = "test";

            // assert
            Assert.False(original.IsWhiteSpace());
        }

        [Fact]
        public void IsWhiteSpaceReturnsTrueForEmpty()
        {
            // arrange
            string original = String.Empty;

            // assert
            Assert.True(original.IsWhiteSpace());
        }

        [Fact]
        public void IsWhiteSpaceReturnsTrueForWhiteSpace()
        {
            // arrange
            string original = " \t";

            // assert
            Assert.True(original.IsWhiteSpace());
        }

        [Fact]
        public void ReplaceIgnoreCaseThrowsArgumentNullException()
        {
            // arrange
            string original = "some string";
            string oldValue = null;
            string newValue = "test";

            // act
            Action act = () => { var replaced = original.ReplaceIgnoreCase(oldValue, newValue); };

            // assert
            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void ReplaceIgnoreCaseThrowsArgumentException()
        {
            // arrange
            string original = "some string";
            string oldValue = String.Empty;
            string newValue = "test";

            // act
            Action act = () => { var replaced = original.ReplaceIgnoreCase(oldValue, newValue); };

            // assert
            Assert.Throws<ArgumentException>(act);
        }

        [Fact]
        public void ReplaceIgnoreCaseOldValueDoesNotOccur()
        {
            // arrange
            string original = "some string";
            string oldValue = "1";
            string newValue = "test";

            // act
            var replaced = original.ReplaceIgnoreCase(oldValue, newValue);

            // assert
            Assert.Equal(replaced, original);
        }

        [Fact]
        public void ReplaceIgnoreCaseOldValueOccursSameCase()
        {
            // arrange
            string original = "some string";
            string oldValue = "some";
            string newValue = "test";

            // act
            var replaced = original.ReplaceIgnoreCase(oldValue, newValue);

            // assert
            Assert.Equal("test string", replaced);
        }

        [Fact]
        public void ReplaceIgnoreCaseOldValueOccursDifferentCase()
        {
            // arrange
            string original = "some string";
            string oldValue = "SOME";
            string newValue = "test";

            // act
            var replaced = original.ReplaceIgnoreCase(oldValue, newValue);

            // assert
            Assert.Equal("test string", replaced);
        }

        [Fact]
        public void ValueOrEmptyStringIfNullReturnsValue()
        {
            // arrange
            string value = "some string";

            // act
            var result = value.ValueOrEmptyStringIfNull();

            // asset
            Assert.Equal("some string", result);
        }

        [Fact]
        public void ValueOrEmptyStringIfNullReturnsEmptyString()
        {
            // arrange
            string value = null;

            // act
            var result = value.ValueOrEmptyStringIfNull();

            // asset
            Assert.Equal(String.Empty, result);
        }

        [Fact]
        public void ValueOrNullStringIfNullReturnsValue()
        {
            // arrange
            string value = "some string";

            // act
            var result = value.ValueOrNullStringIfNull();

            // asset
            Assert.Equal("some string", result);
        }

        [Fact]
        public void ValueOrNullStringIfNullReturnsNull()
        {
            // arrange
            string value = null;

            // act
            var result = value.ValueOrNullStringIfNull();

            // asset
            Assert.Equal("NULL", result);
        }

        [Fact]
        public void ValueOrOtherValueIfNullReturnsValue()
        {
            // arrange
            string value = "some string";

            // act
            var result = value.ValueOrOtherValueIfNull("other string");

            // asset
            Assert.Equal("some string", result);
        }

        [Fact]
        public void ValueOrOtherValueIfNullReturnsOtherValue()
        {
            // arrange
            string value = null;

            // act
            var result = value.ValueOrOtherValueIfNull("other string");

            // asset
            Assert.Equal("other string", result);
        }
    }
}
