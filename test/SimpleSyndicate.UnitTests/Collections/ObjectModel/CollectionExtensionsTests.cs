﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.ObjectModel;
using SimpleSyndicate.Collections.ObjectModel;
using Xunit;

namespace SimpleSyndicate.Tests.Collections.ObjectModel
{
    public class CollectionExtensionsTests
    {
        [Fact]
        public void RemoveByTypeNameThrowsArgumentNullExceptionForCollection()
        {
            // arrange
            Collection<object> collection = null;

            // act
            Action act = () => { collection.RemoveByTypeName("test"); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void RemoveByTypeNameThrowsArgumentNullExceptionForTypeNameToRemove()
        {
            // arrange
            Collection<object> collection = new Collection<object>();

            // act
            Action act = () => { collection.RemoveByTypeName(null); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void RemoveByTypeNameThrowsArgumentException()
        {
            // arrange
            Collection<object> collection = new Collection<object>();

            // act
            Action act = () => { collection.RemoveByTypeName(" "); };

            Assert.Throws<ArgumentException>(act);
        }

        [Fact]
        public void RemoveByTypeNameReturnsTrue()
        {
            // arrange
            Collection<object> collection = new Collection<object>();
            collection.Add("string 1");
            collection.Add(5);
            collection.Add("string 2");

            // act
            var result = collection.RemoveByTypeName("String");

            // assert
            Assert.True(result);
        }

        [Fact]
        public void RemoveByTypeNameRemoves()
        {
            // arrange
            Collection<object> collection = new Collection<object>();
            collection.Add("string 1");
            collection.Add(5);
            collection.Add("string 2");

            // act
            var result = collection.RemoveByTypeName("String");

            // assert
            Assert.Equal(2, collection.Count);
        }

        [Fact]
        public void RemoveByTypeNameReturnsFalse()
        {
            // arrange
            Collection<object> collection = new Collection<object>();
            collection.Add("string 1");
            collection.Add(5);

            // act
            var result = collection.RemoveByTypeName("test");

            // assert
            Assert.False(result);
        }

        [Fact]
        public void RemoveByTypeNameDoesNotRemove()
        {
            // arrange
            Collection<object> collection = new Collection<object>();
            collection.Add("string 1");
            collection.Add(5);

            // act
            var result = collection.RemoveByTypeName("test");

            // assert
            Assert.Equal(2, collection.Count);
        }
    }
}
