﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.ComponentModel.DataAnnotations;
using Xunit;

namespace SimpleSyndicate.Tests
{
    public class AttributeHelpersTests
    {
        public string TestProperty1 { get; set; }

        [Required]
        public string TestProperty2 { get; set; }

        [DataType(DataType.Custom)]
        public int TestMethod()
        {
            return 0;
        }

        [Fact]
        public void GetAttributeThrowsArgumentNullException()
        {
            Action act = () => { AttributeHelpers.GetAttribute(null, typeof(RequiredAttribute)); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void GetMethodAttributeThrowsArgumentNullException()
        {
            Action act = () => { AttributeHelpers.GetAttribute<object, object>(null, typeof(RequiredAttribute)); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void GetMethodAttributeIsNull()
        {
            // arrange
            var model = new AttributeHelpersTests();

            // act
            var attribute = AttributeHelpers.GetAttribute<AttributeHelpersTests, int>(x => x.TestMethod(), typeof(RequiredAttribute));

            // assert
            Assert.Null(attribute);
        }

        [Fact]
        public void GetMethodAttributeIsNotNull()
        {
            // arrange
            var model = new AttributeHelpersTests();

            // act
            var attribute = AttributeHelpers.GetAttribute<AttributeHelpersTests, int>(x => x.TestMethod(), typeof(DataTypeAttribute));

            // assert
            Assert.NotNull(attribute);
        }

        [Fact]
        public void GetPropertyAttributeThrowsArgumentNullException()
        {
            Action act = () => { AttributeHelpers.GetAttribute<object>(null, typeof(RequiredAttribute)); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void GetPropertyAttributeIsNull()
        {
            // arrange
            var model = new AttributeHelpersTests();

            // act
            var attribute = AttributeHelpers.GetAttribute(() => model.TestProperty2, typeof(KeyAttribute));

            // assert
            Assert.Null(attribute);
        }

        [Fact]
        public void GetPropertyAttributeIsNotNull()
        {
            // arrange
            var model = new AttributeHelpersTests();

            // act
            var attribute = AttributeHelpers.GetAttribute(() => model.TestProperty2, typeof(RequiredAttribute));

            // assert
            Assert.NotNull(attribute);
        }

        [Fact]
        public void GetAttributeValueThrowsArgumentNullException()
        {
            Action act = () => { AttributeHelpers.GetAttributeValue<object>(null, typeof(RequiredAttribute)); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void GetAttributeValue()
        {
            // arrange
            var instance = new AttributeHelpersTests();
            instance.TestProperty1 = "1";
            instance.TestProperty2 = "2";

            // act
            var value = AttributeHelpers.GetAttributeValue<int>(instance, typeof(RequiredAttribute));

            // assert
            Assert.Equal(2, value);
        }

        [Fact]
        public void HasMethodAttributeIsFalse()
        {
            // arrange
            var model = new AttributeHelpersTests();

            // act
            var hasAttribute = AttributeHelpers.HasAttribute<AttributeHelpersTests, int>(x => x.TestMethod(), typeof(RequiredAttribute));

            // assert
            Assert.False(hasAttribute);
        }

        [Fact]
        public void HasMethodAttributeIsTrue()
        {
            // arrange
            var model = new AttributeHelpersTests();

            // act
            var hasAttribute = AttributeHelpers.HasAttribute<AttributeHelpersTests, int>(x => x.TestMethod(), typeof(DataTypeAttribute));

            // assert
            Assert.True(hasAttribute);
        }

        [Fact]
        public void HasPropertyAttributeIsFalse()
        {
            // arrange
            var model = new AttributeHelpersTests();

            // act
            var hasAttribute = AttributeHelpers.HasAttribute(() => model.TestProperty2, typeof(KeyAttribute));

            // assert
            Assert.False(hasAttribute);
        }

        [Fact]
        public void HasPropertyAttributeIsTrue()
        {
            // arrange
            var model = new AttributeHelpersTests();

            // act
            var hasAttribute = AttributeHelpers.HasAttribute(() => model.TestProperty2, typeof(RequiredAttribute));

            // assert
            Assert.True(hasAttribute);
        }
    }
}
