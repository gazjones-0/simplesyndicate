﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.IO;
using SimpleSyndicate.IO;
using Xunit;

namespace SimpleSyndicate.Tests.IO
{
    public class FileHelpersTests
    {
        [Fact]
        public void ChangeExtensionThrowsArgumentNullExceptionForPath()
        {
            Action act = () => { FileHelpers.ChangeExtension(null, null); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void ChangeExtensionThrowsArgumentNullExceptionForExtension()
        {
            Action act = () => { FileHelpers.ChangeExtension("something", null); };

            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void ChangeExtensionThrowsFileNotFoundException()
        {
            Action act = () => { FileHelpers.ChangeExtension("invalidpath", "newExtension"); };

            Assert.Throws<FileNotFoundException>(act);
        }

        [Fact]
        public void ChangeExtension()
        {
            // arrange
            var temp = Path.GetTempFileName();
            var oldExtension = new FileInfo(temp).Extension;
            var oldFile = new FileInfo(temp).FullName;
            var newExtension = oldExtension + "1";
            var newFile = oldFile.Substring(0, oldFile.Length - oldExtension.Length) + newExtension;

            // act
            FileHelpers.ChangeExtension(temp, newExtension);

            // assert
            Assert.False(File.Exists(oldFile));
            Assert.True(File.Exists(newFile));

            // cleanup
            File.Delete(newFile);
        }

        [Fact]
        public void ChangeExtensionNoExtension()
        {
            // arrange
            var temp = Path.GetTempFileName();
            var oldExtension = new FileInfo(temp).Extension;
            var oldFile = new FileInfo(temp).FullName;
            var newExtension = String.Empty;
            var newFile = oldFile.Substring(0, oldFile.Length - oldExtension.Length) + newExtension;

            // act
            FileHelpers.ChangeExtension(temp, newExtension);

            // assert
            Assert.False(File.Exists(oldFile));
            Assert.True(File.Exists(newFile));

            // cleanup
            File.Delete(newFile);
        }

        [Fact]
        public void ChangeExtensionNoDot()
        {
            // arrange
            var temp = Path.GetTempFileName();
            var oldExtension = new FileInfo(temp).Extension;
            var oldFile = new FileInfo(temp).FullName;
            var newExtension = oldExtension + "1";
            var newFile = oldFile.Substring(0, oldFile.Length - oldExtension.Length) + newExtension;

            // act
            FileHelpers.ChangeExtension(temp, newExtension.Substring(1));

            // assert
            Assert.False(File.Exists(oldFile));
            Assert.True(File.Exists(newFile));

            // cleanup
            File.Delete(newFile);
        }

        [Fact]
        public void ChangeExtensionNoChange()
        {
            // arrange
            var temp = Path.GetTempFileName();
            var oldExtension = new FileInfo(temp).Extension;
            var oldFile = new FileInfo(temp).FullName;
            var newExtension = oldExtension;
            var newFile = oldFile.Substring(0, oldFile.Length - oldExtension.Length) + newExtension;

            // act
            FileHelpers.ChangeExtension(temp, newExtension);

            // assert
            Assert.True(File.Exists(oldFile));
            Assert.True(File.Exists(newFile));

            // cleanup
            File.Delete(newFile);
        }
    }
}
