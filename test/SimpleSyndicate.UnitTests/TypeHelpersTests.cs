﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using Xunit;

namespace SimpleSyndicate.Tests
{
    public class TypeHelpersTests
    {
        [Fact]
        public void FullNameFromCodeDomThrowsArgumentNullException()
        {
            Action act = () => { var name = TypeHelpers.FullNameFromCodeDom(null); };
            Assert.Throws<ArgumentNullException>(act);
        }

        [Fact]
        public void FullNameFromCodeDomReturnsName()
        {
            // arrange
            Repositories.IRepository<string> type = new NotImplementedRepository<string>();

            // act
            var name = TypeHelpers.FullNameFromCodeDom(type.GetType());

            var fullname = TypeHelpers.FullNameFromCodeDom(type.GetType());

            // assert
            Assert.Equal("SimpleSyndicate.Tests.NotImplementedRepository<string>", name);
        }
    }
}
