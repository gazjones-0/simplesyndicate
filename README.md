# README #

SimpleSyndicate NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate

### What is this repository for? ###

* Common functionality for .Net applications - generic repository interface, helper methods for working with attributes, built-in types, etc.

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate/issues?status=new&status=open
