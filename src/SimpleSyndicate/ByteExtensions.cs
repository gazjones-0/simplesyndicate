﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Text;

namespace SimpleSyndicate
{
    /// <summary>
    /// The <see cref="ByteExtensions"/> class contains methods that extend the <see cref="byte"/> class.
    /// </summary>
    public static class ByteExtensions
    {
        /// <summary>
        /// Decodes the contents as an ASCII string.
        /// </summary>
        /// <param name="array">The <see cref="byte"/> array instance that this method extends.</param>
        /// <returns>The contents decoded as an ASCII string.</returns>
        public static string ToASCII(this byte[] array)
        {
            var result = Encoding.ASCII.GetString(array);
            if (result.StartsWith(Encoding.ASCII.GetString(Encoding.ASCII.GetPreamble()), StringComparison.OrdinalIgnoreCase))
            {
                result = result.Remove(0, Encoding.ASCII.GetString(Encoding.ASCII.GetPreamble()).Length);
            }

            return result;
        }

        /// <summary>
        /// Decodes the contents as a Big Endian Unicode string.
        /// </summary>
        /// <param name="array">The <see cref="byte"/> array instance that this method extends.</param>
        /// <returns>The contents decoded as a Big Endian Unicode string.</returns>
        public static string ToBigEndianUnicode(this byte[] array)
        {
            var result = Encoding.BigEndianUnicode.GetString(array);
            if (result.StartsWith(Encoding.BigEndianUnicode.GetString(Encoding.BigEndianUnicode.GetPreamble()), StringComparison.OrdinalIgnoreCase))
            {
                result = result.Remove(0, Encoding.BigEndianUnicode.GetString(Encoding.BigEndianUnicode.GetPreamble()).Length);
            }

            return result;
        }

        /// <summary>
        /// Decodes the contents as a Unicode string.
        /// </summary>
        /// <param name="array">The <see cref="byte"/> array instance that this method extends.</param>
        /// <returns>The contents decoded as a Unicode string.</returns>
        public static string ToUnicode(this byte[] array)
        {
            var result = Encoding.Unicode.GetString(array);
            if (result.StartsWith(Encoding.Unicode.GetString(Encoding.Unicode.GetPreamble()), StringComparison.OrdinalIgnoreCase))
            {
                result = result.Remove(0, Encoding.Unicode.GetString(Encoding.Unicode.GetPreamble()).Length);
            }

            return result;
        }

        /// <summary>
        /// Decodes the contents as a UTF32 string.
        /// </summary>
        /// <param name="array">The <see cref="byte"/> array instance that this method extends.</param>
        /// <returns>The contents decoded as a UTF32 string.</returns>
        public static string ToUTF32(this byte[] array)
        {
            var result = Encoding.UTF32.GetString(array);
            if (result.StartsWith(Encoding.UTF32.GetString(Encoding.UTF32.GetPreamble()), StringComparison.OrdinalIgnoreCase))
            {
                result = result.Remove(0, Encoding.UTF32.GetString(Encoding.UTF32.GetPreamble()).Length);
            }

            return result;
        }

        /// <summary>
        /// Decodes the contents as a UTF7 string.
        /// </summary>
        /// <param name="array">The <see cref="byte"/> array instance that this method extends.</param>
        /// <returns>The contents decoded as a UTF7 string.</returns>
        public static string ToUTF7(this byte[] array)
        {
            var result = Encoding.UTF7.GetString(array);
            if (result.StartsWith(Encoding.UTF7.GetString(Encoding.UTF7.GetPreamble()), StringComparison.OrdinalIgnoreCase))
            {
                result = result.Remove(0, Encoding.UTF7.GetString(Encoding.UTF7.GetPreamble()).Length);
            }

            return result;
        }

        /// <summary>
        /// Decodes the contents as a UTF8 string.
        /// </summary>
        /// <param name="array">The <see cref="byte"/> array instance that this method extends.</param>
        /// <returns>The contents decoded as a UTF8 string.</returns>
        public static string ToUTF8(this byte[] array)
        {
            var result = Encoding.UTF8.GetString(array);
            if (result.StartsWith(Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble()), StringComparison.OrdinalIgnoreCase))
            {
                result = result.Remove(0, Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble()).Length);
            }

            return result;
        }

        /// <summary>
        /// Decodes the contents to a string, trying different encodings until it finds one that results in a string that starts with the specified <paramref name="expectedToStartWith"/>,
        /// which would indicate a successful decoding.
        /// </summary>
        /// <param name="array">The <see cref="byte"/> array instance that this method extends.</param>
        /// <param name="expectedToStartWith">What the contents are expected to start with if successfully decoded.</param>
        /// <returns>The contents decoded to a string.</returns>
        /// <exception cref="InvalidOperationException">Thrown when none of the encodings result in a string that starts with <paramref name="expectedToStartWith"/>.</exception>
        public static string ToString(this byte[] array, string expectedToStartWith)
        {
            var result = array.ToASCII();
            if (!result.StartsWith(expectedToStartWith, StringComparison.OrdinalIgnoreCase))
            {
                result = array.ToBigEndianUnicode();
            }

            if (!result.StartsWith(expectedToStartWith, StringComparison.OrdinalIgnoreCase))
            {
                result = array.ToUnicode();
            }

            if (!result.StartsWith(expectedToStartWith, StringComparison.OrdinalIgnoreCase))
            {
                result = array.ToUTF32();
            }

            if (!result.StartsWith(expectedToStartWith, StringComparison.OrdinalIgnoreCase))
            {
                result = array.ToUTF7();
            }

            if (!result.StartsWith(expectedToStartWith, StringComparison.OrdinalIgnoreCase))
            {
                result = array.ToUTF8();
            }

            if (result.StartsWith(expectedToStartWith, StringComparison.OrdinalIgnoreCase))
            {
                return result;
            }

            throw new InvalidOperationException();
        }
    }
}
