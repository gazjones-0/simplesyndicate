﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.IO;
using System.Reflection;

namespace SimpleSyndicate
{
    /// <summary>
    /// Helper methods for working with resources.
    /// </summary>
    public static class ResourceHelpers
    {
        /// <summary>
        /// Returns the resource with the specified <paramref name="resourceName"/>, or <c>null</c> if it can't be found; three searches
        /// will be preformed -- the specified name, it prefixed with the entry assembly and it prefixed with the entry assembly and
        /// '.Resources'; for example, in an app called 'MyApp', a search for 'MyResource' would first look for 'MyResource', then for
        /// 'MyApp.MyResource' and finally for 'MyApp.Resources.MyResource'.
        /// </summary>
        /// <param name="resourceName">Resource name.</param>
        /// <returns>The resource, or <c>null</c> if it can't be found.</returns>
        public static byte[] GetByteArray(string resourceName)
        {
            if (resourceName == null)
            {
                throw new ArgumentNullException(nameof(resourceName));
            }

            if (string.IsNullOrWhiteSpace(resourceName))
            {
                throw new ArgumentException("Resource name cannot be blank or whitespace", nameof(resourceName));
            }

            var assembly = Assembly.GetExecutingAssembly();
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                if (stream != null)
                {
                    using (var memory = new MemoryStream())
                    {
                        stream.CopyTo(memory);
                        return memory.ToArray();
                    }
                }
            }

            using (var stream = assembly.GetManifestResourceStream(PrefixResourceNameWithAssembly(resourceName, assembly)))
            {
                if (stream != null)
                {
                    using (var memory = new MemoryStream())
                    {
                        stream.CopyTo(memory);
                        return memory.ToArray();
                    }
                }
            }

            using (var stream = assembly.GetManifestResourceStream(PrefixResourceNameWithAssembly("Resources." + resourceName, assembly)))
            {
                if (stream != null)
                {
                    using (var memory = new MemoryStream())
                    {
                        stream.CopyTo(memory);
                        return memory.ToArray();
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the resource with the specified <paramref name="resourceName"/>, or <c>null</c> if it can't be found; three searches
        /// will be preformed -- the specified name, it prefixed with the entry assembly and it prefixed with the entry assembly and
        /// '.Resources'; for example, in an app called 'MyApp', a search for 'MyResource' would first look for 'MyResource', then for
        /// 'MyApp.MyResource' and finally for 'MyApp.Resources.MyResource'.
        /// </summary>
        /// <param name="resourceName">Resource name.</param>
        /// <returns>The resource, or <c>null</c> if it can't be found.</returns>
        public static string GetString(string resourceName)
        {
            if (resourceName == null)
            {
                throw new ArgumentNullException(nameof(resourceName));
            }

            if (string.IsNullOrWhiteSpace(resourceName))
            {
                throw new ArgumentException("Resource name cannot be blank or whitespace", nameof(resourceName));
            }

            var assembly = Assembly.GetEntryAssembly();
            using (var stream = assembly.GetManifestResourceStream(resourceName))
            {
                if (stream != null)
                {
                    using (var reader = new StreamReader(stream, true))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }

            using (var stream = assembly.GetManifestResourceStream(PrefixResourceNameWithAssembly(resourceName, assembly)))
            {
                if (stream != null)
                {
                    using (var reader = new StreamReader(stream, true))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }

            using (var stream = assembly.GetManifestResourceStream(PrefixResourceNameWithAssembly("Resources." + resourceName, assembly)))
            {
                if (stream != null)
                {
                    using (var reader = new StreamReader(stream, true))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Prefixes the resource name with the assembly's name (but without any trailing '.dll' or '.exe' in the assembly name).
        /// </summary>
        /// <param name="resourceName">Resource name.</param>
        /// <param name="assembly">Assembly name.</param>
        /// <returns>Prefixed resource name.</returns>
        private static string PrefixResourceNameWithAssembly(string resourceName, Assembly assembly)
        {
            var name = assembly.GetName().Name;
            if (name.EndsWith(".dll", StringComparison.OrdinalIgnoreCase) || name.EndsWith(".exe", StringComparison.OrdinalIgnoreCase))
            {
                name = name.Substring(0, name.Length - 4);
            }

            return name + "." + resourceName;
        }
    }
}
