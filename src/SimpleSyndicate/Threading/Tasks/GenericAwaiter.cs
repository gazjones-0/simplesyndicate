﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Runtime.CompilerServices;

namespace SimpleSyndicate.Threading.Tasks
{
    /// <summary>
    /// Generic awaiter to allow awaiting on objects that wouldn't normally support it, for example <code>await new SomeObject();</code>.
    /// </summary>
    /// <typeparam name="TSource">Type being awaited.</typeparam>
    public class GenericAwaiter<TSource> : INotifyCompletion
        where TSource : class
    {
        private readonly TSource source;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericAwaiter{TSource}" /> class.
        /// </summary>
        /// <param name="source">Object being awaited.</param>
        public GenericAwaiter(TSource source)
        {
            this.source = source ?? throw new ArgumentNullException(nameof(source));
            IsCompleted = false;
        }

        /// <summary>
        /// Gets a value indicating whether the operation has completed.
        /// </summary>
        /// <value>Whether the operation has completed.</value>
        public bool IsCompleted { get; private set; }

        /// <summary>
        /// Schedules the continuation action to be invoked when the instance completes.
        /// </summary>
        /// <param name="continuation">The action to invoke when the operation completes.</param>
        public void OnCompleted(Action continuation)
        {
            continuation?.Invoke();
        }

#pragma warning disable CA1024 // Use properties where appropriate
        /// <summary>
        /// Returns the object that was awated on.
        /// </summary>
        /// <returns>The object awaited on.</returns>
        public TSource GetResult()
#pragma warning restore CA1024 // Use properties where appropriate
        {
            IsCompleted = true;
            return source;
        }
    }
}
