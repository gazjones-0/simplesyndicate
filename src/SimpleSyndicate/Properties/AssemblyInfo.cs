﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Reflection;
using System.Runtime.InteropServices;

// general information
[assembly: AssemblyTitle("SimpleSyndicate")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("simpleSyndicate")]
[assembly: AssemblyProduct("SimpleSyndicate")]
[assembly: AssemblyCopyright("Copyright © simpleSyndicate 2014, 2015, 2016, 2017, 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// version
[assembly: AssemblyVersion("2.0.7")]
[assembly: AssemblyFileVersion("2.0.7")]

// CLS compliance
[assembly: CLSCompliant(true)]

// COM visibility
[assembly: ComVisible(false)]
