﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Text.RegularExpressions;

namespace SimpleSyndicate
{
    /// <summary>
    /// The <see cref="StringExtensions"/> class contains methods that extend the <see cref="string"/> class.
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Returns whether the specified string ends with <paramref name="value"/>.
        /// </summary>
        /// <param name="source">The string to test.</param>
        /// <param name="value">The character to compare.</param>
        /// <returns><c>true</c> if the string ends with <paramref name="value"/>; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static bool EndsWith(this string source, char value)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return source.EndsWith(value, StringComparison.Ordinal);
        }

        /// <summary>
        /// Returns whether the specified string ends with <paramref name="value"/>.
        /// </summary>
        /// <param name="source">The string to test.</param>
        /// <param name="value">The character to compare.</param>
        /// <param name="comparisonType">One of the enumeration values that specifies how the comparison is performed.</param>
        /// <returns><c>true</c> if the string ends with <paramref name="value"/>; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static bool EndsWith(this string source, char value, StringComparison comparisonType)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Right(1).Equals(value.ToString(System.Globalization.CultureInfo.InvariantCulture), comparisonType))
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Indicates whether the specified string is empty.
        /// </summary>
        /// <param name="source">The string to test.</param>
        /// <returns><c>true</c> if the string is empty, <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static bool IsEmpty(this string source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return string.IsNullOrEmpty(source);
        }

        /// <summary>
        /// Indicates whether a specified string is empty or consists only of white-space characters.
        /// </summary>
        /// <param name="source">The string to test.</param>
        /// <returns><c>true</c> if the string is empty or consists only of white-space characters, <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static bool IsWhiteSpace(this string source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return string.IsNullOrWhiteSpace(source);
        }

        /// <summary>
        /// Returns the left-most <paramref name="length"/> characters from <paramref name="source"/>; if <paramref name="length"/> is
        /// more than the length of <paramref name="source"/>, <paramref name="source"/> is returned.
        /// </summary>
        /// <param name="source">The string instance that this method extends.</param>
        /// <param name="length">The number of characters in the substring.</param>
        /// <returns>THe left-most <paramref name="length"/> characters from <paramref name="source"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when <paramref name="length"/> is less than zero.</exception>
        public static string Left(this string source, int length)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            if (length > source.Length)
            {
                length = source.Length;
            }

            return source.Substring(0, length);
        }

        /// <summary>
        /// Returns a new string in which all occurrences of a specified string in the current instance are replaced with another specified string.
        /// </summary>
        /// <param name="source">The string instance that this method extends.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to replace all occurrences of <paramref name="oldValue"/>.</param>
        /// <returns>A string that is equivalent to the current string except that all instances of <paramref name="oldValue"/> are replaced
        /// with <paramref name="newValue"/>. If <paramref name="oldValue"/> is not found in the current instance, the method returns the
        /// current instance unchanged.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> or <paramref name="oldValue"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="oldValue"/> is the empty string ("").</exception>
        public static string ReplaceIgnoreCase(this string source, string oldValue, string newValue)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (oldValue == null)
            {
                throw new ArgumentNullException(nameof(oldValue));
            }

            if (string.IsNullOrEmpty(oldValue))
            {
                throw new ArgumentException("oldValue is the empty string (\"\").", nameof(oldValue));
            }

            return Regex.Replace(source, Regex.Escape(oldValue), Regex.Replace(newValue, "\\$[0-9]+", @"$$$0"), RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// Returns the right-most <paramref name="length"/> characters from <paramref name="source"/>; if <paramref name="length"/> is
        /// more than the length of <paramref name="source"/>, <paramref name="source"/> is returned.
        /// </summary>
        /// <param name="source">The string instance that this method extends.</param>
        /// <param name="length">The number of characters in the substring.</param>
        /// <returns>THe right-most <paramref name="length"/> characters from <paramref name="source"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Thrown when <paramref name="length"/> is less than zero.</exception>
        public static string Right(this string source, int length)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (length < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            if (length > source.Length)
            {
                length = source.Length;
            }

            return source.Substring(source.Length - length);
        }

        /// <summary>
        /// Returns the number of times the specified <paramref name="value"/> occurs within a string.
        /// </summary>
        /// <param name="source">The string instance that this method extends.</param>
        /// <param name="value">The <c>char</c> to search for.</param>
        /// <returns>The number of times <paramref name="value"/> occurs.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static int Occurrences(this string source, char value)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            var length = source.Length;
            int count = 0;
            for (int index = length - 1; index >= 0; index--)
            {
                if (source[index] == value)
                {
                    count++;
                }
            }

            return count;
        }

        /// <summary>
        /// Returns the number of times the specified <paramref name="value"/> occurs within a string.
        /// </summary>
        /// <param name="source">The string instance that this method extends.</param>
        /// <param name="value">The string to search for.</param>
        /// <returns>The number of times <paramref name="value"/> occurs.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static int Occurrences(this string source, string value)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (!string.IsNullOrWhiteSpace(value))
            {
                return (source.Length - source.Replace(value, string.Empty).Length) / value.Length;
            }

            return 0;
        }

        /// <summary>
        /// Converts the string to the specified <typeparamref name="TEnum"/> enumeration.
        /// </summary>
        /// <typeparam name="TEnum">The enumeration to convert the <paramref name="source"/> to.</typeparam>
        /// <param name="source">The <see cref="string"/> instance that this method extends.</param>
        /// <returns>The <paramref name="source"/> converted to the <typeparamref name="TEnum"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        /// <exception cref="InvalidOperationException">Thrown if <paramref name="source"/> cannot be converted.</exception>
        public static TEnum ToEnum<TEnum>(this string source)
            where TEnum : struct
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            TEnum result;
            if (Enum.TryParse<TEnum>(source, true, out result))
            {
                return result;
            }

            throw new InvalidOperationException("Failed to convert source (\"" + source + "\") to enumeration");
        }

        /// <summary>
        /// Converts the string to the specified <typeparamref name="TEnum"/> enumeration, or <paramref name="defaultValue"/> if it cannot be converted.
        /// </summary>
        /// <typeparam name="TEnum">The enumeration type to convert the <paramref name="source"/> to.</typeparam>
        /// <param name="source">The <see cref="string"/> instance that this method extends.</param>
        /// <param name="defaultValue">The value to return if the <paramref name="source"/> cannot be converted.</param>
        /// <returns>The <paramref name="source"/> converted to the <typeparamref name="TEnum"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static TEnum ToEnum<TEnum>(this string source, TEnum defaultValue)
            where TEnum : struct
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            TEnum result;
            if (Enum.TryParse<TEnum>(source, true, out result))
            {
                return result;
            }

            return defaultValue;
        }

        /// <summary>
        /// Converts the string to a <see cref="VersionComponent"/>, or <see cref="VersionComponent.Unknown"/> if it cannot be converted.
        /// </summary>
        /// <param name="source">The <see cref="string"/> instance that this method extends.</param>
        /// <returns>The <paramref name="source"/> converted to a <see cref="VersionComponent"/>.</returns>
        public static VersionComponent ToVersionComponent(this string source)
        {
            return VersionComponentFactory.Create(source);
        }

        /// <summary>
        /// If the string is <c>null</c>, returns <c>String.Empty</c>; otherwise it returns the string.
        /// </summary>
        /// <param name="source">The <see cref="string"/> instance that this method extends.</param>
        /// <returns><c>String.Empty</c> if <paramref name="source"/> is <c>null</c>; otherwise <paramref name="source"/>.</returns>
        public static string ValueOrEmptyStringIfNull(this string source)
        {
            return source.ValueOrOtherValueIfNull(string.Empty);
        }

        /// <summary>
        /// If the string is <c>null</c>, returns the string "NULL"; otherwise it returns the string.
        /// </summary>
        /// <param name="source">The <see cref="string"/> instance that this method extends.</param>
        /// <returns>"NULL" if <paramref name="source"/> is <c>null</c>; otherwise <paramref name="source"/>.</returns>
        public static string ValueOrNullStringIfNull(this string source)
        {
            return source.ValueOrOtherValueIfNull("NULL");
        }

        /// <summary>
        /// If the string is <c>null</c>, returns <paramref name="valueIfNull"/>; otherwise it returns the string.
        /// </summary>
        /// <param name="source">The <see cref="string"/> instance that this method extends.</param>
        /// <param name="valueIfNull">The string to return if <paramref name="source"/> is <c>null</c>.</param>
        /// <returns><paramref name="valueIfNull"/> if <paramref name="source"/> is <c>null</c>; otherwise <paramref name="source"/>.</returns>
        public static string ValueOrOtherValueIfNull(this string source, string valueIfNull)
        {
            if (source != null)
            {
                return source;
            }

            return valueIfNull;
        }
    }
}
