﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate.Diagnostics
{
    /// <summary>
    /// Specifies a set of values holding the details of a process that ran.
    /// </summary>
    public class ProcessExitInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessExitInfo"/> class with the specified <paramref name="standardError"/> and <paramref name="standardOutput"/>.
        /// </summary>
        /// <param name="standardError">Standard error.</param>
        /// <param name="standardOutput">Standard output.</param>
        public ProcessExitInfo(string standardError, string standardOutput)
        {
            Exception = null;
            StandardError = standardError;
            StandardOutput = standardOutput;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessExitInfo"/> class with the specified <paramref name="standardError"/> and <paramref name="standardOutput"/>.
        /// </summary>
        /// <param name="standardError">Standard error.</param>
        /// <param name="standardOutput">Standard output.</param>
        /// <param name="exception">Exception caught whilst the process ran.</param>
        public ProcessExitInfo(string standardError, string standardOutput, Exception exception)
        {
            Exception = exception;
            StandardError = standardError;
            StandardOutput = standardOutput;
        }

        /// <summary>
        /// Gets any exception caught whilst the process ran; if no exception was caught, this will be <c>null</c>.
        /// </summary>
        /// <value>Exception caught whilst the process ran; if no exception was caught, this will be <c>null</c>.</value>
        public Exception Exception { get; private set; }

        /// <summary>
        /// Gets the standard error contents from the process ran.
        /// </summary>
        /// <value>Standard error contents.</value>
        public string StandardError { get; private set; }

        /// <summary>
        /// Gets the standard output contents from the process ran.
        /// </summary>
        /// <value>Standard output contents.</value>
        public string StandardOutput { get; private set; }
    }
}
