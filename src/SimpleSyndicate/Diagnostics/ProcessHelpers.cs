﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Diagnostics;

namespace SimpleSyndicate.Diagnostics
{
    /// <summary>
    /// Helper methods for working with processes.
    /// </summary>
    public static class ProcessHelpers
    {
        /// <summary>
        /// Runs a console application.
        /// </summary>
        /// <param name="workingDirectory">Working directory.</param>
        /// <param name="fileName">Application to run.</param>
        /// <returns>The results of the run.</returns>
        public static ProcessExitInfo RunConsoleApp(string workingDirectory, string fileName)
        {
            return RunConsoleApp(workingDirectory, fileName, string.Empty);
        }

        /// <summary>
        /// Runs a console application.
        /// </summary>
        /// <param name="workingDirectory">Working directory.</param>
        /// <param name="fileName">Application to run.</param>
        /// <param name="arguments">Arguments for the application.</param>
        /// <returns>The results of the run.</returns>
        public static ProcessExitInfo RunConsoleApp(string workingDirectory, string fileName, string arguments)
        {
            using (var process = new Process())
            {
                if (!string.IsNullOrWhiteSpace(workingDirectory))
                {
                    process.StartInfo.WorkingDirectory = workingDirectory;
                }

                process.StartInfo.FileName = fileName;
                if (!string.IsNullOrEmpty(arguments))
                {
                    process.StartInfo.Arguments = arguments;
                }

                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardError = true;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.CreateNoWindow = true;
                string stderr = null;
                string stdout = null;
                try
                {
                    process.Start();
                    stderr = process.StandardError.ReadToEnd();
                    stdout = process.StandardOutput.ReadToEnd();
                    process.WaitForExit();
                    return new ProcessExitInfo(stderr, stdout);
                }
                catch (Exception e)
                {
                    return new ProcessExitInfo(stderr, stdout, e);
                }
            }
        }
    }
}
