﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace SimpleSyndicate.IO
{
    /// <summary>
    /// Helper methods for working with files.
    /// </summary>
    public static class FileHelpers
    {
        /// <summary>
        /// Changes the extension of a file.
        /// </summary>
        /// <param name="path">File to change.</param>
        /// <param name="newExtension">New file extension.</param>
        /// <exception cref="FileNotFoundException">Thrown when the file to change cannot be found.</exception>
        public static void ChangeExtension(string path, string newExtension)
        {
            if (newExtension == null)
            {
                throw new ArgumentNullException(nameof(newExtension));
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }

            var oldFile = new FileInfo(path);
            var newFullName = Path.ChangeExtension(oldFile.FullName, newExtension);

            if (string.Compare(oldFile.FullName, newFullName, StringComparison.Ordinal) != 0)
            {
                File.Move(oldFile.FullName, newFullName);
            }
        }

        /// <summary>
        /// Modifies a file by performing the specified operation on its contents.
        /// </summary>
        /// <param name="path">File to modify.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <returns><c>true</c> if any modification occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> or <paramref name="operation"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        public static bool ModifyText(string path, Func<string, string> operation)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("path must not be empty or whitespace", nameof(path));
            }

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            return ModifyTextInternal(path, operation, null, null);
        }

        /// <summary>
        /// Modifies a file by performing the specified operation on its contents.
        /// </summary>
        /// <param name="path">File to modify.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any modification occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="operation"/> or <paramref name="encoding"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        public static bool ModifyText(string path, Func<string, string> operation, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("path must not be empty or whitespace", nameof(path));
            }

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            return ModifyTextInternal(path, operation, null, encoding);
        }

        /// <summary>
        /// Modifies a file by performing the specified operation on its contents.
        /// </summary>
        /// <param name="path">File to modify.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="postOperation">Operation to perform after the file has been operated on; the path, old contents and new contexts are passed in.</param>
        /// <returns><c>true</c> if any modification occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="operation"/> or <paramref name="postOperation"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        public static bool ModifyText(string path, Func<string, string> operation, Action<string, string, string> postOperation)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("path must not be empty or whitespace", nameof(path));
            }

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            if (postOperation == null)
            {
                throw new ArgumentNullException(nameof(postOperation));
            }

            return ModifyTextInternal(path, operation, postOperation, null);
        }

        /// <summary>
        /// Modifies a file by performing the specified operation on its contents.
        /// </summary>
        /// <param name="path">File to modify.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="postOperation">Operation to perform after the file has been operated on; the path, old contents and new contexts are passed in.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any modification occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="operation"/> or <paramref name="encoding"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        public static bool ModifyText(string path, Func<string, string> operation, Action<string, string, string> postOperation, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("path must not be empty or whitespace", nameof(path));
            }

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            if (postOperation == null)
            {
                throw new ArgumentNullException(nameof(postOperation));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            return ModifyTextInternal(path, operation, postOperation, encoding);
        }

        /// <summary>
        /// Modifies file(s) by performing the specified operation on the contents of files found from the specified <paramref name="path"/> and <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">Path to start the search at.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="searchPattern">Search pattern; if <c>null</c> or empty all files are processed.</param>
        /// <returns><c>true</c> if any modifications occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> or <paramref name="operation"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        /// <exception cref="DirectoryNotFoundException">Thrown when <paramref name="path"/> can't be found.</exception>
        public static bool ModifyText(string path, Func<string, string> operation, string searchPattern)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path.ToString(CultureInfo.InvariantCulture));
            }

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            return ModifyTextInternal(path, operation, searchPattern, null, null);
        }

        /// <summary>
        /// Modifies file(s) by performing the specified operation on the contents of files found from the specified <paramref name="path"/> and <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">Path to start the search at.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="searchPattern">Search pattern; if <c>null</c> or empty all files are processed.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any modifications occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="operation"/> or <paramref name="encoding"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        /// <exception cref="DirectoryNotFoundException">Thrown when <paramref name="path"/> can't be found.</exception>
        public static bool ModifyText(string path, Func<string, string> operation, string searchPattern, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path.ToString(CultureInfo.InvariantCulture));
            }

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            return ModifyTextInternal(path, operation, searchPattern, null, encoding);
        }

        /// <summary>
        /// Modifies file(s) by performing the specified operation on the contents of files found from the specified <paramref name="path"/> and <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">Path to start the search at.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="searchPattern">Search pattern; if <c>null</c> or empty all files are processed.</param>
        /// <param name="postOperation">Operation to perform after the file has been operated on; the path, old contents and new contexts are passed in.</param>
        /// <returns><c>true</c> if any modifications occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> or <paramref name="operation"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        /// <exception cref="DirectoryNotFoundException">Thrown when <paramref name="path"/> can't be found.</exception>
        public static bool ModifyText(string path, Func<string, string> operation, string searchPattern, Action<string, string, string> postOperation)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path.ToString(CultureInfo.InvariantCulture));
            }

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            if (postOperation == null)
            {
                throw new ArgumentNullException(nameof(postOperation));
            }

            return ModifyTextInternal(path, operation, searchPattern, postOperation, null);
        }

        /// <summary>
        /// Modifies file(s) by performing the specified operation on the contents of files found from the specified <paramref name="path"/> and <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">Path to start the search at.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="searchPattern">Search pattern; if <c>null</c> or empty all files are processed.</param>
        /// <param name="postOperation">Operation to perform after the file has been operated on; the path, old contents and new contexts are passed in.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any modifications occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="operation"/> or <paramref name="encoding"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        /// <exception cref="DirectoryNotFoundException">Thrown when <paramref name="path"/> can't be found.</exception>
        public static bool ModifyText(string path, Func<string, string> operation, string searchPattern, Action<string, string, string> postOperation, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path.ToString(CultureInfo.InvariantCulture));
            }

            if (operation == null)
            {
                throw new ArgumentNullException(nameof(operation));
            }

            if (postOperation == null)
            {
                throw new ArgumentNullException(nameof(postOperation));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            return ModifyTextInternal(path, operation, searchPattern, postOperation, encoding);
        }

        /// <summary>
        /// Prepends the specified file with the specified <paramref name="contents"/>, unless the file already starts with <paramref name="contents"/>, in which case it is left unchanged.
        /// </summary>
        /// <param name="path">File to prepend.</param>
        /// <param name="contents">The string to prepend the file with.</param>
        /// <returns><c>true</c> if the file was prepended; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> or <paramref name="contents"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        public static bool PrependText(string path, string contents)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("path must not be empty or whitespace", nameof(path));
            }

            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }

            return ModifyText(
                path,
                (oldContents) =>
                {
                    if (!oldContents.StartsWith(contents, StringComparison.Ordinal))
                    {
                        return contents + oldContents;
                    }
                    return oldContents;
                });
        }

        /// <summary>
        /// Prepends the specified file with the specified <paramref name="contents"/>, unless the file already starts with <paramref name="contents"/>, in which case it is left unchanged.
        /// </summary>
        /// <param name="path">File to prepend.</param>
        /// <param name="contents">The string to prepend the file with.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if the file was prepended; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="contents"/> or <paramref name="encoding"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace.</exception>
        public static bool PrependText(string path, string contents, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("path must not be empty or whitespace", nameof(path));
            }

            if (contents == null)
            {
                throw new ArgumentNullException(nameof(contents));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            return ModifyText(
                path,
                (oldContents) =>
                {
                    if (!oldContents.StartsWith(contents, StringComparison.Ordinal))
                    {
                        return contents + oldContents;
                    }
                    return oldContents;
                },
                encoding);
        }

        /// <summary>
        /// Replaces all occurrences of the specified <paramref name="oldValue"/> with <paramref name="newValue"/> in the specified file.
        /// </summary>
        /// <param name="path">File to change.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to replace all occurrences of <paramref name="oldValue"/>.</param>
        /// <returns><c>true</c> if any replaces occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="oldValue"/> or <paramref name="newValue"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace, or <paramref name="oldValue"/> is empty.</exception>
        public static bool ReplaceText(string path, string oldValue, string newValue)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("path must not be empty or whitespace", nameof(path));
            }

            if (oldValue == null)
            {
                throw new ArgumentNullException(nameof(oldValue));
            }

            if (string.IsNullOrEmpty(oldValue))
            {
                throw new ArgumentException("oldValue must not be empty", nameof(oldValue));
            }

            if (newValue == null)
            {
                throw new ArgumentNullException(nameof(newValue));
            }

            return ModifyText(path, (oldContents) => { return oldContents.Replace(oldValue, newValue); });
        }

        /// <summary>
        /// Replaces all occurrences of the specified <paramref name="oldValue"/> with <paramref name="newValue"/> in the specified file.
        /// </summary>
        /// <param name="path">File to change.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to replace all occurrences of <paramref name="oldValue"/>.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any replaces occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="oldValue"/>, <paramref name="newValue"/> or <paramref name="encoding"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace, or <paramref name="oldValue"/> is empty.</exception>
        public static bool ReplaceText(string path, string oldValue, string newValue, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("path must not be empty or whitespace", nameof(path));
            }

            if (oldValue == null)
            {
                throw new ArgumentNullException(nameof(oldValue));
            }

            if (string.IsNullOrEmpty(oldValue))
            {
                throw new ArgumentException("oldValue must not be empty", nameof(oldValue));
            }

            if (newValue == null)
            {
                throw new ArgumentNullException(nameof(newValue));
            }

            if (encoding == null)
            {
                throw new ArgumentNullException(nameof(encoding));
            }

            return ModifyText(path, (oldContents) => { return oldContents.Replace(oldValue, newValue); }, encoding);
        }

        /// <summary>
        /// Replaces all occurrences of the specified <paramref name="oldValue"/> with <paramref name="newValue"/> in the files
        /// found from the specified <paramref name="path"/> and <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">Path to start search at.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to replace all occurrences of <paramref name="oldValue"/>.</param>
        /// <param name="searchPattern">Search pattern; if <c>null</c> or empty all files are processed.</param>
        /// <returns><c>true</c> if any replaces occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="oldValue"/> or <paramref name="newValue"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace, or <paramref name="oldValue"/> is empty.</exception>
        /// <exception cref="DirectoryNotFoundException">Thrown when <paramref name="path"/> can't be found.</exception>
        public static bool ReplaceText(string path, string oldValue, string newValue, string searchPattern)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path.ToString(CultureInfo.InvariantCulture));
            }

            if (oldValue == null)
            {
                throw new ArgumentNullException(nameof(oldValue));
            }

            if (string.IsNullOrEmpty(oldValue))
            {
                throw new ArgumentException("oldValue must not be empty", nameof(oldValue));
            }

            if (newValue == null)
            {
                throw new ArgumentNullException(nameof(newValue));
            }

            return ModifyText(path, (oldContents) => { return oldContents.Replace(oldValue, newValue); }, searchPattern);
        }

        /// <summary>
        /// Replaces all occurrences of the specified <paramref name="oldValue"/> with <paramref name="newValue"/> in the files
        /// found from the specified <paramref name="path"/> and <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">Path to start search at.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to replace all occurrences of <paramref name="oldValue"/>.</param>
        /// <param name="searchPattern">Search pattern; if <c>null</c> or empty all files are processed.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any replaces occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="oldValue"/>,  <paramref name="newValue"/> or <paramref name="encoding"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace, or <paramref name="oldValue"/> is empty.</exception>
        /// <exception cref="DirectoryNotFoundException">Thrown when <paramref name="path"/> can't be found.</exception>
        public static bool ReplaceText(string path, string oldValue, string newValue, string searchPattern, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path.ToString(CultureInfo.InvariantCulture));
            }

            if (oldValue == null)
            {
                throw new ArgumentNullException(nameof(oldValue));
            }

            if (string.IsNullOrEmpty(oldValue))
            {
                throw new ArgumentException("oldValue must not be empty", nameof(oldValue));
            }

            if (newValue == null)
            {
                throw new ArgumentNullException(nameof(newValue));
            }

            return ModifyText(path, (oldContents) => { return oldContents.Replace(oldValue, newValue); }, searchPattern, encoding);
        }

        /// <summary>
        /// Replaces all occurrences of the specified <paramref name="oldValue"/> with <paramref name="newValue"/> in the files
        /// found from the specified <paramref name="path"/> and <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">Path to start search at.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to replace all occurrences of <paramref name="oldValue"/>.</param>
        /// <param name="searchPattern">Search pattern; if <c>null</c> or empty all files are processed.</param>
        /// <param name="postOperation">Operation to perform after the file has been operated on; the path, old contents and new contexts are passed in.</param>
        /// <returns><c>true</c> if any replaces occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="oldValue"/>, <paramref name="newValue"/> or <paramref name="postOperation"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace, or <paramref name="oldValue"/> is empty.</exception>
        /// <exception cref="DirectoryNotFoundException">Thrown when <paramref name="path"/> can't be found.</exception>
        public static bool ReplaceText(string path, string oldValue, string newValue, string searchPattern, Action<string, string, string> postOperation)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path.ToString(CultureInfo.InvariantCulture));
            }

            if (oldValue == null)
            {
                throw new ArgumentNullException(nameof(oldValue));
            }

            if (string.IsNullOrEmpty(oldValue))
            {
                throw new ArgumentException("oldValue must not be empty", nameof(oldValue));
            }

            if (newValue == null)
            {
                throw new ArgumentNullException(nameof(newValue));
            }

            if (postOperation == null)
            {
                throw new ArgumentNullException(nameof(postOperation));
            }

            return ModifyText(path, (oldContents) => { return oldContents.Replace(oldValue, newValue); }, searchPattern, postOperation);
        }

        /// <summary>
        /// Replaces all occurrences of the specified <paramref name="oldValue"/> with <paramref name="newValue"/> in the files
        /// found from the specified <paramref name="path"/> and <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">Path to start search at.</param>
        /// <param name="oldValue">The string to be replaced.</param>
        /// <param name="newValue">The string to replace all occurrences of <paramref name="oldValue"/>.</param>
        /// <param name="searchPattern">Search pattern; if <c>null</c> or empty all files are processed.</param>
        /// <param name="postOperation">Operation to perform after the file has been operated on; the path, old contents and new contexts are passed in.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any replaces occurred; <c>false</c> otherwise.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/>, <paramref name="oldValue"/>,  <paramref name="newValue"/>, <paramref name="encoding"/> or <paramref name="postOperation"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> is empty or whitespace, or <paramref name="oldValue"/> is empty.</exception>
        /// <exception cref="DirectoryNotFoundException">Thrown when <paramref name="path"/> can't be found.</exception>
        public static bool ReplaceText(string path, string oldValue, string newValue, string searchPattern, Action<string, string, string> postOperation, Encoding encoding)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!Directory.Exists(path))
            {
                throw new DirectoryNotFoundException(path.ToString(CultureInfo.InvariantCulture));
            }

            if (oldValue == null)
            {
                throw new ArgumentNullException(nameof(oldValue));
            }

            if (string.IsNullOrEmpty(oldValue))
            {
                throw new ArgumentException("oldValue must not be empty", nameof(oldValue));
            }

            if (newValue == null)
            {
                throw new ArgumentNullException(nameof(newValue));
            }

            if (postOperation == null)
            {
                throw new ArgumentNullException(nameof(postOperation));
            }

            return ModifyText(path, (oldContents) => { return oldContents.Replace(oldValue, newValue); }, searchPattern, postOperation, encoding);
        }

        /// <summary>
        /// Modifies a file by performing the specified operation on its contents.
        /// </summary>
        /// <param name="path">File to modify.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="postOperation">Operation to perform after the file has been operated on; the path, old contents and new contexts are passed in.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any modification occurred; <c>false</c> otherwise.</returns>
        private static bool ModifyTextInternal(string path, Func<string, string> operation, Action<string, string, string> postOperation, Encoding encoding)
        {
            var oldContents = encoding != null ? File.ReadAllText(path, encoding) : File.ReadAllText(path);
            var newContents = operation(oldContents);
            var result = false;
            if (string.Compare(oldContents, newContents, StringComparison.Ordinal) != 0)
            {
                if (encoding != null)
                {
                    File.WriteAllText(path, newContents, encoding);
                }
                else
                {
                    File.WriteAllText(path, newContents);
                }

                result = true;
            }

            postOperation?.Invoke(path, oldContents, newContents);

            return result;
        }

        /// <summary>
        /// Modifies files by performing the specified operation on the contents of files found from the specified path and search pattern.
        /// </summary>
        /// <param name="path">Path to start the search at.</param>
        /// <param name="operation">Operation to perform on the file contents; the existing contents are passed in, and the modified ones are returned.</param>
        /// <param name="searchPattern">Search pattern.</param>
        /// <param name="postOperation">Operation to perform after the file has been operated on; the path, old contents and new contexts are passed in.</param>
        /// <param name="encoding">The character encoding to use.</param>
        /// <returns><c>true</c> if any modifications occurred; <c>false</c> otherwise.</returns>
        private static bool ModifyTextInternal(string path, Func<string, string> operation, string searchPattern, Action<string, string, string> postOperation, Encoding encoding)
        {
            var directory = new DirectoryInfo(path);
            bool result = false;
            foreach (var subDirectory in directory.GetDirectories())
            {
                var subDirectoryResult = ModifyTextInternal(subDirectory.FullName, operation, searchPattern, postOperation, encoding);
                result = subDirectoryResult == true ? true : result;
            }

            FileInfo[] files;
            if (string.IsNullOrEmpty(searchPattern))
            {
                files = directory.GetFiles();
            }
            else
            {
                files = directory.GetFiles(searchPattern);
            }

            foreach (var file in files)
            {
                var fileResult = ModifyTextInternal(file.FullName, operation, postOperation, encoding);
                result = fileResult == true ? true : result;
            }

            return result;
        }
    }
}
