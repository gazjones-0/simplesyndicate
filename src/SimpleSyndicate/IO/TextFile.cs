﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.IO;

namespace SimpleSyndicate.IO
{
    /// <summary>
    /// Provides easy access to, and manipulation of, a text file.
    /// </summary>
    public class TextFile : TextFileBase
    {
        /// <summary>
        /// The contents of the file when it was last loaded.
        /// </summary>
        private string _contentWhenLastLoaded = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextFile"/> class using the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to the file.</param>
        public TextFile(string path)
            : base(path)
        {
        }

        /// <summary>
        /// Gets the lines of the file.
        /// </summary>
        /// <value>The lines.</value>
        public IList<string> Lines { get; private set; }

        /// <summary>
        /// Loads the file.
        /// </summary>
        protected override void LoadFile()
        {
            if (Encoding != null)
            {
                Lines = new List<string>(File.ReadAllLines(FullName, Encoding));
            }
            else
            {
                Lines = new List<string>(File.ReadAllLines(FullName));
            }

            _contentWhenLastLoaded = string.Join("\n", Lines);
        }

        /// <summary>
        /// Saves the file.
        /// </summary>
        protected override void SaveFile()
        {
            if (Lines == null)
            {
                throw new InvalidOperationException("Can't save file as no file was loaded.");
            }

            if (string.Compare(_contentWhenLastLoaded, string.Join("\n", Lines), StringComparison.Ordinal) != 0)
            {
                if (Encoding != null)
                {
                    File.WriteAllLines(FullName, Lines, Encoding);
                }
                else
                {
                    File.WriteAllLines(FullName, Lines);
                }
            }
        }
    }
}
