﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System.IO;

namespace SimpleSyndicate.IO
{
    /// <summary>
    /// Provides easy access to, and manipulation of, a file that is always treated as a string.
    /// </summary>
    public class StringFile : TextFileBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StringFile"/> class using the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to the file.</param>
        public StringFile(string path)
            : base(path)
        {
        }

        /// <summary>
        /// Gets or sets the contents of the file.
        /// </summary>
        /// <value>Contents of the file.</value>
        public string Contents { get; set; }

        /// <summary>
        /// Loads the file.
        /// </summary>
        protected override void LoadFile()
        {
            if (Encoding != null)
            {
                Contents = File.ReadAllText(System.IO.Path.Combine(Path, FileName), Encoding);
            }
            else
            {
                Contents = File.ReadAllText(System.IO.Path.Combine(Path, FileName));
            }
        }

        /// <summary>
        /// Saves the file.
        /// </summary>
        protected override void SaveFile()
        {
            if (Encoding != null)
            {
                File.WriteAllText(System.IO.Path.Combine(Path, FileName), Contents, Encoding);
            }
            else
            {
                File.WriteAllText(System.IO.Path.Combine(Path, FileName), Contents);
            }
        }
    }
}
