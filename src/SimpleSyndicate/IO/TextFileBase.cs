﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SimpleSyndicate.IO
{
    /// <summary>
    /// Provides easy access to, and manipulation of, a text file; the encoding will be preserved when the file is saved.
    /// </summary>
    public abstract class TextFileBase
    {
        /// <summary>
        /// Whether a file has been loaded.
        /// </summary>
        private bool _fileLoaded = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="TextFileBase"/> class using the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to the file.</param>
        protected TextFileBase(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (!File.Exists(path))
            {
                throw new FileNotFoundException(path);
            }

            FileName = path;
            Path = System.IO.Path.GetDirectoryName(FileName);
            FileName = System.IO.Path.GetFileName(FileName);
            Load();
        }

        /// <summary>
        /// Gets the encoding detected when the file was loaded.
        /// </summary>
        /// <value>Encoding of the file.</value>
        public Encoding Encoding { get; private set; }

        /// <summary>
        /// Gets the path to the file.
        /// </summary>
        /// <value>Path to the file.</value>
        public string Path { get; private set; }

        /// <summary>
        /// Gets the name of the file.
        /// </summary>
        /// <value>Filename.</value>
        public string FileName { get; private set; }

        /// <summary>
        /// Gets the full path of the file.
        /// </summary>
        /// <value>Full path of the file.</value>
        public string FullName => System.IO.Path.Combine(Path, FileName);

        /// <summary>
        /// Loads the file.
        /// </summary>
        public void Load()
        {
            // flag no file loaded as we might fail to load it
            _fileLoaded = false;
            Encoding = null;

            // try and determine the encoding
            using (var reader = new StreamReader(FullName, true))
            {
                // need to do a peek so part of the file is read, otherwise the encoding won't be detected
                reader.Peek();
                Encoding = reader.CurrentEncoding;
            }

            LoadFile();
            OnLoad();
        }

        /// <summary>
        /// Saves the file.
        /// </summary>
        public void Save()
        {
            if (_fileLoaded == false)
            {
                throw new InvalidOperationException("Can't save file as no file was loaded.");
            }

            SaveFile();
        }

        /// <summary>
        /// Does the actual loading of the file, implemented by derived classes.
        /// </summary>
        protected abstract void LoadFile();

        /// <summary>
        /// Called whenever the file is loaded.
        /// </summary>
        protected virtual void OnLoad()
        {
        }

        /// <summary>
        /// Does the actual saving of the file, implemented by derived classes.
        /// </summary>
        protected abstract void SaveFile();
    }
}
