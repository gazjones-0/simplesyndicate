﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Globalization;
using System.IO;

namespace SimpleSyndicate.IO
{
    /// <summary>
    /// Helper methods for working with <see cref="Directory"/>s.
    /// </summary>
    public static class DirectoryHelpers
    {
        /// <summary>
        /// Returns whether the specified <paramref name="path"/> contains a directory with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="path">The path to search in.</param>
        /// <param name="name">The string to compare to each directory name.</param>
        /// <returns><c>true</c> if the specified <paramref name="path"/> contains a directory with the specified <paramref name="name"/>; <c>false</c> otherwise.</returns>
        public static bool ContainsDirectory(string path, string name)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            foreach (var directory in Directory.EnumerateDirectories(path))
            {
                if (LastDirectoryName(directory).Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns whether the specified <paramref name="path"/> contains a file with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="path">The path to search in.</param>
        /// <param name="name">The string to compare to each file name.</param>
        /// <returns><c>true</c> if the specified <paramref name="path"/> contains a file with the specified <paramref name="name"/>; <c>false</c> otherwise.</returns>
        public static bool ContainsFile(string path, string name)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return ContainsFile(path, name, false);
        }

        /// <summary>
        /// Returns whether the specified <paramref name="path"/> contains a file with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="path">The path to search in.</param>
        /// <param name="name">The string to compare to each file name.</param>
        /// <param name="recurse"><c>true</c> to do a recursive search; <c>false</c> otherwise.</param>
        /// <returns><c>true</c> if the specified <paramref name="path"/> contains a file with the specified <paramref name="name"/>; <c>false</c> otherwise.</returns>
        public static bool ContainsFile(string path, string name, bool recurse)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            var file = PathIfContainsFile(path, name, recurse);
            if (file != null)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns whether the specified <paramref name="path"/> contains a file with a name that ends in <paramref name="endsWith"/>.
        /// </summary>
        /// <param name="path">THe path to search in.</param>
        /// <param name="endsWith">The string to compare to each file name.</param>
        /// <returns><c>true</c> if the specified <paramref name="path"/> contains a file with a name that ends in <paramref name="endsWith"/>; <c>false</c> otherwise.</returns>
        public static bool ContainsFileWithNameThatEndsWith(string path, string endsWith)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (endsWith == null)
            {
                throw new ArgumentNullException(nameof(endsWith));
            }

            foreach (var file in Directory.EnumerateFiles(path))
            {
                if (Path.GetFileName(file).EndsWith(endsWith, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns whether the specified <paramref name="path"/> ends with a directory separator.
        /// </summary>
        /// <param name="path">Path to check.</param>
        /// <returns><c>true</c> if the <paramref name="path"/> ends with a directory separator; <c>false</c> otherwise.</returns>
        public static bool EndsWithDirectorySeparator(string path)
        {
            if (path != null)
            {
                if (path.EndsWith(Path.DirectorySeparatorChar, StringComparison.OrdinalIgnoreCase)
                    || path.EndsWith(Path.AltDirectorySeparatorChar, StringComparison.OrdinalIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Returns the specified <paramref name="path"/> if it is a file that exists, otherwise uses <paramref name="path"/> as a directory and returns the first
        /// file found that ends with the specified <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">File or path to examine.</param>
        /// <param name="searchPattern">Pattern to match against.</param>
        /// <returns>The provided <paramref name="path"/> or the first file found (including the path) that matches the <paramref name="searchPattern"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> or <paramref name="searchPattern"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> or <paramref name="searchPattern"/> are empty or whitespace.</exception>
        /// <exception cref="FileNotFoundException">Thrown when no file is found.</exception>
        public static string FileOrFirstMatchingFile(string path, string searchPattern)
        {
            return FileOrFirstMatchingFile(path, searchPattern, null);
        }

        /// <summary>
        /// Returns the specified <paramref name="path"/> if it is a file that exists, otherwise uses <paramref name="path"/> as a directory and returns the first
        /// file found that ends with the specified <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">File or path to examine.</param>
        /// <param name="searchPattern">Pattern to match against.</param>
        /// <param name="wrapperExceptionFactory">Factory method that will create the exception to throw if the file cannot be found; a <see cref="FileNotFoundException"/> exception is passed to the method.</param>
        /// <returns>The provided <paramref name="path"/> or the first file found (including the path) that matches the <paramref name="searchPattern"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> or <paramref name="searchPattern"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> or <paramref name="searchPattern"/> are empty or whitespace.</exception>
        public static string FileOrFirstMatchingFile(string path, string searchPattern, Func<Exception, Exception> wrapperExceptionFactory)
        {
            var file = FileOrFirstMatchingFileOrNull(path, searchPattern);
            if (file == null)
            {
                var exception = new FileNotFoundException(string.Format(CultureInfo.CurrentCulture, "\"{0}\" is not a file; no file that ends with \"{1}\" found when treated as a directory.", path, searchPattern));
                if (wrapperExceptionFactory == null)
                {
                    throw exception;
                }

                throw wrapperExceptionFactory(exception);
            }

            return file;
        }

        /// <summary>
        /// Returns the specified <paramref name="path"/> if it is a file that exists, otherwise uses <paramref name="path"/> as a directory and returns the first
        /// file found that ends with the specified <paramref name="searchPattern"/>.
        /// </summary>
        /// <param name="path">File or path to examine.</param>
        /// <param name="searchPattern">Pattern to match against.</param>
        /// <returns>The provided <paramref name="path"/> or the first file found (including the path) that matches the <paramref name="searchPattern"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> or <paramref name="searchPattern"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="path"/> or <paramref name="searchPattern"/> are empty or whitespace.</exception>
        public static string FileOrFirstMatchingFileOrNull(string path, string searchPattern)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Path must not be empty or whitespace.", nameof(path));
            }

            if (searchPattern == null)
            {
                throw new ArgumentNullException(nameof(searchPattern));
            }

            if (string.IsNullOrWhiteSpace(path))
            {
                throw new ArgumentException("Search pattern must not be empty or whitespace.", nameof(searchPattern));
            }

            // if the path we've been given is a file, that's the file
            if (System.IO.File.Exists(path))
            {
                return path;
            }

            // not a file, so assume it's a directory and go with the first file we find
            foreach (var file in System.IO.Directory.EnumerateFiles(path))
            {
                if (Path.GetFileName(file).EndsWith(searchPattern, StringComparison.OrdinalIgnoreCase))
                {
                    return file;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns the last directory name in the specified <paramref name="path"/>; note that the <paramref name="path"/> must not include the filename.
        /// </summary>
        /// <param name="path">Path, not including the filename, to process.</param>
        /// <returns>The last directory name in the <paramref name="path"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> is <c>null</c>.</exception>
        public static string LastDirectoryName(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            return LastDirectoryName(path, false);
        }

        /// <summary>
        /// Returns the last directory name in the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">Path to process.</param>
        /// <param name="pathIncludesFileName"><c>true</c> if the <paramref name="path"/> includes the file name; <c>false</c> otherwise.</param>
        /// <returns>The last directory name in the <paramref name="path"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="path"/> is <c>null</c>.</exception>
        public static string LastDirectoryName(string path, bool pathIncludesFileName)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            var processedPath = path;
            if (pathIncludesFileName == true)
            {
                processedPath = Path.GetDirectoryName(processedPath);
            }

            while (EndsWithDirectorySeparator(processedPath))
            {
                processedPath = processedPath.Left(processedPath.Length - 1);
            }

            return Path.GetFileName(processedPath);
        }

        /// <summary>
        /// Returns the path if a specified <paramref name="path"/> contains a file with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="path">The path to search in.</param>
        /// <param name="name">The string to compare to each file name.</param>
        /// <param name="recurse"><c>true</c> to do a recursive search; <c>false</c> otherwise.</param>
        /// <returns>The path, or <c>null</c> if it can't be found.</returns>
        public static string PathIfContainsFile(string path, string name, bool recurse)
        {
            if (path == null)
            {
                throw new ArgumentNullException(nameof(path));
            }

            if (name == null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            if (recurse == true)
            {
                foreach (var directory in Directory.EnumerateDirectories(path))
                {
                    var fileInDirectory = PathIfContainsFile(directory, name, recurse);
                    if (fileInDirectory != null)
                    {
                        return fileInDirectory;
                    }
                }
            }

            foreach (var file in Directory.EnumerateFiles(path))
            {
                if (Path.GetFileName(file).Equals(name, StringComparison.OrdinalIgnoreCase))
                {
                    return Path.Combine(path, file);
                }
            }

            return null;
        }
    }
}
