﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.ObjectModel;

namespace SimpleSyndicate.Collections.ObjectModel
{
    /// <summary>
    /// The <see cref="CollectionExtensions"/> class contains methods that extend the <see cref="Collection{T}"/> class.
    /// </summary>
    public static class CollectionExtensions
    {
        /// <summary>
        /// Removes the first occurrence of an item with a type of the specified <paramref name="typeNameToRemove"/>.
        /// </summary>
        /// <typeparam name="T">The type of elements in the collection.</typeparam>
        /// <param name="collection">The <see cref="Collection{T}"/> instance that this method extends.</param>
        /// <param name="typeNameToRemove">Name of type to remove.</param>
        /// <returns><c>true</c> if an item with specified <paramref name="typeNameToRemove"/> was successfully removed; <c>false</c> otherwise. This method also returns <c>false</c> if no item was found in the collection.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="collection"/> or <paramref name="typeNameToRemove"/> are <c>null</c>.</exception>
        /// <exception cref="ArgumentException">Thrown when <paramref name="typeNameToRemove"/> is empty or whitespace.</exception>
        public static bool RemoveByTypeName<T>(this Collection<T> collection, string typeNameToRemove)
            where T : class
        {
            if (collection == null)
            {
                throw new ArgumentNullException(nameof(collection));
            }

            if (typeNameToRemove == null)
            {
                throw new ArgumentNullException(nameof(typeNameToRemove));
            }

            if (string.IsNullOrWhiteSpace(typeNameToRemove))
            {
                throw new ArgumentException("typeNameToRemove specifying type to remove must be non-whitespace", nameof(typeNameToRemove));
            }

            T foundItem = null;
            foreach (var item in collection)
            {
                if (string.CompareOrdinal(item.GetType().Name, typeNameToRemove) == 0)
                {
                    foundItem = item;
                }
            }

            if (foundItem != null)
            {
                return collection.Remove(foundItem);
            }

            return false;
        }
    }
}
