﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate
{
    /// <summary>
    /// Version component.
    /// </summary>
    public enum VersionComponent
    {
        /// <summary>
        /// Unknown component.
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// Major component.
        /// </summary>
        Major = 0,

        /// <summary>
        /// Minor component.
        /// </summary>
        Minor = 1,

        /// <summary>
        /// Build component
        /// </summary>
        Build = 2,

        /// <summary>
        /// Revision component.
        /// </summary>
        Revision = 3
    }
}
