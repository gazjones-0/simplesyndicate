﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Linq.Expressions;
using System.Reflection;

namespace SimpleSyndicate
{
    /// <summary>
    /// Helper methods for working with <see cref="Attribute"/>s.
    /// </summary>
    public static class AttributeHelpers
    {
        /// <overloads>
        /// <summary>Gets the attribute of the specified type from the specified expression.</summary>
        /// </overloads>
        /// <summary>
        /// Gets the attribute of type <paramref name="type"/> from the specified <paramref name="expression"/>.
        /// </summary>
        /// <param name="expression">Lambda expression.</param>
        /// <param name="type"><see cref="Type"/> of attribute to find.</param>
        /// <returns>The attribute of the specified <paramref name="type"/>, or <c>null</c> if it doesn't exist.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="expression"/> is <c>null</c>.</exception>
        public static Attribute GetAttribute(Expression<Action> expression, Type type)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            MethodCallExpression body = (MethodCallExpression)expression.Body;
            return body.Method.GetCustomAttribute(type);
        }

        /// <summary>
        /// Gets the attribute of type <typeparamref name="TAttribute"/> from the property specified by <paramref name="expression"/>.
        /// </summary>
        /// <example>
        /// <code language="cs">
        /// var model = new SomeMvcController();
        /// var attribute = AttributeHelpers.GetAttribute&lt;SomeMvcController, ViewResult&gt;(x => x.Index(), typeof(AllowAnonymousAttribute));
        /// </code>
        /// </example>
        /// <typeparam name="TClass">Class that contains a property.</typeparam>
        /// <typeparam name="TAttribute">Attribute to find.</typeparam>
        /// <param name="expression">Lambda expression to the property.</param>
        /// <returns>The specified attribute, or <c>null</c> if it doesn't exist on the property.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="expression"/> is <c>null</c>.</exception>
        public static TAttribute GetAttribute<TClass, TAttribute>(Expression<Func<TClass, object>> expression)
            where TAttribute : Attribute
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            Attribute attribute = AttributeHelpers.GetAttribute(expression, typeof(TAttribute));
            return (TAttribute)attribute;
        }

        /// <summary>
        /// Gets the attribute of type <paramref name="type"/> from the method specified by <paramref name="expression"/>.
        /// </summary>
        /// <example>
        /// <code language="cs">
        /// var model = new SomeMvcController();
        /// var attribute = AttributeHelpers.GetAttribute&lt;SomeMvcController, ViewResult&gt;(x => x.Index(), typeof(AllowAnonymousAttribute));
        /// </code>
        /// </example>
        /// <typeparam name="TClass">Class that defines the method.</typeparam>
        /// <typeparam name="TValue">Return type of the method.</typeparam>
        /// <param name="expression">Lambda expression to the method.</param>
        /// <param name="type"><see cref="Type"/> of attribute to find.</param>
        /// <returns>The attribute of the specified <paramref name="type"/>, or <c>null</c> if it doesn't exist on the method.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="expression"/> is <c>null</c>.</exception>
        public static Attribute GetAttribute<TClass, TValue>(Expression<Func<TClass, TValue>> expression, Type type)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            if (expression.Body is MethodCallExpression)
            {
                var methodCall = (MethodCallExpression)expression.Body;
                return methodCall.Method.GetCustomAttribute(type);
            }

            var method = (MemberExpression)expression.Body;
            if (expression.Body is UnaryExpression)
            {
                method = (MemberExpression)((UnaryExpression)expression.Body).Operand;
            }

            return method.Member.GetCustomAttribute(type);
        }

        /// <summary>
        /// Gets the attribute of type <paramref name="type"/> from the property specified by <paramref name="expression"/>.
        /// </summary>
        /// <example>
        /// <code language="cs">
        /// var model = new VersionHistoryItem();
        /// var attribute = AttributeHelpers.GetAttribute(() => model.MajorVersion, typeof(KeyAttribute));
        /// </code>
        /// </example>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="expression">Lambda expression to the property.</param>
        /// <param name="type"><see cref="Type"/> of attribute to find.</param>
        /// <returns>The attribute of the specified <paramref name="type"/>, or <c>null</c> if it doesn't exist on the property.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="expression"/> is <c>null</c>.</exception>
        public static Attribute GetAttribute<TProperty>(Expression<Func<TProperty>> expression, Type type)
        {
            if (expression == null)
            {
                throw new ArgumentNullException(nameof(expression));
            }

            var method = (MemberExpression)expression.Body;
            if (expression.Body is UnaryExpression)
            {
                method = (MemberExpression)((UnaryExpression)expression.Body).Operand;
            }

            return method.Member.GetCustomAttribute(type);
        }

        /// <summary>
        /// Returns the value of the property on the specified <paramref name="instance"/> that has an attribute of type <paramref name="type"/>.
        /// </summary>
        /// <example>
        /// <code language="cs">
        /// var versionHistoryItem = new VersionHistoryItem();
        /// var value = AttributeHelpers.GetAttributeValue&lt;int&gt;(versionHistoryItem, typeof(KeyAttribute));
        /// // value holds the value of versionHistoryItem.Id
        /// </code>
        /// </example>
        /// <typeparam name="TType">Property type.</typeparam>
        /// <param name="instance">Object to retrieve property value from.</param>
        /// <param name="type">Attribute type to look for.</param>
        /// <returns>The value, as <typeparamref name="TType"/> of the property on <paramref name="instance"/> that has an attribute of type <paramref name="type"/>.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="instance"/> is <c>null</c>.</exception>
        public static TType GetAttributeValue<TType>(object instance, Type type)
        {
            if (instance == null)
            {
                throw new ArgumentNullException(nameof(instance));
            }

            foreach (var property in instance.GetType().GetProperties())
            {
                var attribute = property.GetCustomAttribute(type);
                if (attribute != null)
                {
                    var value = property.GetValue(instance);
                    if (value != null)
                    {
                        var converter = System.ComponentModel.TypeDescriptor.GetConverter(typeof(TType));
                        return (TType)converter.ConvertFrom(value);
                    }
                }
            }

            return default(TType);
        }

        /// <overloads>
        /// <summary>Returns whether the specified specified expression has an attribute of the specified type.</summary>
        /// </overloads>
        /// <summary>
        /// Returns whether the specified <paramref name="expression"/> has an attribute of the specified <paramref name="type"/>.
        /// </summary>
        /// <param name="expression">Lambda expression.</param>
        /// <param name="type"><see cref="Type"/> of attribute to find.</param>
        /// <returns><c>true</c> if the <paramref name="expression"/> has the attribute, <c>false</c> otherwise.</returns>
        public static bool HasAttribute(Expression<Action> expression, Type type)
        {
            return GetAttribute(expression, type) != null ? true : false;
        }

        /// <summary>
        /// Returns whether the method specified by <paramref name="expression"/> has an attribute of type <paramref name="type"/>.
        /// </summary>
        /// <example>
        /// <code language="cs">
        /// var model = new SomeMvcController();
        /// var hasAttribute = AttributeHelpers.HasAttribute&lt;SomeMvcController, ViewResult&gt;(x => x.Index(), typeof(AllowAnonymousAttribute));
        /// </code>
        /// </example>
        /// <typeparam name="TClass">Class that defines the method.</typeparam>
        /// <typeparam name="TValue">Return type of the method.</typeparam>
        /// <param name="expression">Lambda expression to the method.</param>
        /// <param name="type"><see cref="Type"/> of attribute to find.</param>
        /// <returns><c>true</c> if the method has the attribute, <c>false</c> otherwise.</returns>
        public static bool HasAttribute<TClass, TValue>(Expression<Func<TClass, TValue>> expression, Type type)
        {
            return GetAttribute(expression, type) != null ? true : false;
        }

        /// <summary>
        /// Returns where the property specified by <paramref name="expression"/> has an attribute of type <paramref name="type"/> .
        /// </summary>
        /// <example>
        /// <code language="cs">
        /// var model = new VersionHistoryItem();
        /// var hasAttribute = AttributeHelpers.HasAttribute(() => model.MajorVersion, typeof(KeyAttribute));
        /// </code>
        /// </example>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="expression">Lambda expression to the property.</param>
        /// <param name="type"><see cref="Type"/> of attribute to find.</param>
        /// <returns><c>true</c> if the property has the attribute, <c>false</c> otherwise.</returns>
        public static bool HasAttribute<TProperty>(Expression<Func<TProperty>> expression, Type type)
        {
            return GetAttribute(expression, type) != null ? true : false;
        }
    }
}
