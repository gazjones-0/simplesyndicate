﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

namespace SimpleSyndicate
{
    /// <summary>
    /// Factory class for creating <see cref="VersionComponent"/>.
    /// </summary>
    public static class VersionComponentFactory
    {
        /// <summary>
        /// Converts the string to a <see cref="VersionComponent"/>, or <see cref="VersionComponent.Unknown"/> if it cannot be converted.
        /// </summary>
        /// <param name="source">The string to convert.</param>
        /// <returns>The <paramref name="source"/> converted to a <see cref="VersionComponent"/>.</returns>
        public static VersionComponent Create(string source)
        {
            if (source == null)
            {
                return VersionComponent.Unknown;
            }

            switch (source.ToUpperInvariant())
            {
                case "MAJOR":
                    return VersionComponent.Major;
                case "MINOR":
                    return VersionComponent.Minor;
                case "BUILD":
                case "PATCH":
                case "POINT":
                    return VersionComponent.Build;
                case "PRE-RELEASE":
                case "PRERELEASE":
                case "REVISION":
                    return VersionComponent.Revision;
            }

            return VersionComponent.Unknown;
        }
    }
}
