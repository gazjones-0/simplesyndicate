﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate
{
    /// <summary>
    /// The <see cref="VersionExtensions"/> class contains methods that extend the <see cref="Version"/> class.
    /// </summary>
    public static class VersionExtensions
    {
        /// <summary>
        /// Increments the major version by 1, setting the minor, build and revision to zero.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <returns>A new <see cref="Version"/> with the major version incremented by 1.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version IncrementMajor(this Version source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new Version(source.Major + 1, 0, 0, 0);
        }

        /// <summary>
        /// Increments the minor version by 1, setting the build and revision to zero.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <returns>A new <see cref="Version"/> with the minor version incremented by 1.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version IncrementMinor(this Version source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new Version(source.Major, source.Minor + 1, 0, 0);
        }

        /// <summary>
        /// Increments the build number by 1, setting the revision to zero; if the build number is undefined it is treated as zero.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <returns>A new <see cref="Version"/> with the build number incremented by 1, or 1 if it was undefined.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version IncrementBuild(this Version source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Build == -1)
            {
                return new Version(source.Major, source.Minor, 1, 0);
            }

            return new Version(source.Major, source.Minor, source.Build + 1, 0);
        }

        /// <summary>
        /// Increments the revision by 1; if the revision is undefined it is treated as zero.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <returns>A new <see cref="Version"/> with the revision incremented by 1, or 1 if it was undefined.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version IncrementRevision(this Version source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            if (source.Revision == -1)
            {
                return new Version(source.Major, source.Minor, source.Build, 1);
            }

            return new Version(source.Major, source.Minor, source.Build, source.Revision + 1);
        }

        /// <summary>
        /// Removes the build number by setting it to undefined; the revision is also set to undefined.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <returns>A new <see cref="Version"/> with the build number and revision set to undefined.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version RemoveBuild(this Version source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new Version(source.Major, source.Minor);
        }

        /// <summary>
        /// Removes the revision by setting it to undefined.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <returns>A new <see cref="Version"/> with the revision set to undefined.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version RemoveRevision(this Version source)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new Version(source.Major, source.Minor, source.Build);
        }

        /// <summary>
        /// Sets the major version to the specified value.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <param name="major">Major version.</param>
        /// <returns>A new <see cref="Version"/> with the major version set to the specified value.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version SetMajor(this Version source, int major)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new Version(major, source.Minor, source.Build, source.Revision);
        }

        /// <summary>
        /// Sets the minor version to the specified value.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <param name="minor">Minor version.</param>
        /// <returns>A new <see cref="Version"/> with the minor version set to the specified value.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version SetMinor(this Version source, int minor)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new Version(source.Major, minor, source.Build, source.Revision);
        }

        /// <summary>
        /// Sets the build number to the specified value.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <param name="build">Build number.</param>
        /// <returns>A new <see cref="Version"/> with the build number set to the specified value.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version SetBuild(this Version source, int build)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new Version(source.Major, source.Minor, build, source.Revision);
        }

        /// <summary>
        /// Sets the revision to the specified value.
        /// </summary>
        /// <param name="source">The <see cref="Version"/> instance that this method extends.</param>
        /// <param name="revision">The revision.</param>
        /// <returns>A new <see cref="Version"/> with the revision set to the specified value.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="source"/> is <c>null</c>.</exception>
        public static Version SetRevision(this Version source, int revision)
        {
            if (source == null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            return new Version(source.Major, source.Minor, source.Build, revision);
        }
    }
}
