﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Linq;
using System.Linq.Expressions;

namespace SimpleSyndicate.Repositories
{
    /// <summary>
    /// Generic repository interface.
    /// </summary>
    /// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
    public interface IRepository<TEntity> : IDisposable
        where TEntity : class
    {
        /// <overloads>
        /// <summary>
        /// Gets all the entities in the repository.
        /// <note type="important">
        /// It is strongly recommended that <see cref="All{TProjection}"/> be used rather than <see cref="All" /> to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Gets all the entities in the repository.
        /// <note type="important">
        /// As every field of every entity will be returned, it is strongly recommended that <see cref="All{TProjection}"/> be used instead to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// <returns>An <see cref="IQueryable"/> of all the entities.</returns>
        IQueryable<TEntity> All();

        /// <summary>
        /// Gets all the entities in the repository, projecting them to type <typeparamref name="TProjection"/>.
        /// <note type="note">
        /// Only the fields that actually get projected will be queried from the underlying data store, improving performance.
        /// </note>
        /// </summary>
        /// <typeparam name="TProjection">Type to project the entities to.</typeparam>
        /// <returns>An <see cref="IQueryable"/> of all the projected entities.</returns>
        IQueryable<TProjection> All<TProjection>();

        /// <summary>
        /// Returns the number of entities in the repository.
        /// </summary>
        /// <returns>The number of entities in the repository.</returns>
        int Count();

        /// <overloads>
        /// <summary>
        /// Finds an entity.
        /// <note type="important">
        /// It is strongly recommended that <see cref="Find{TProjection}"/> be used rather than <see cref="Find" /> to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// </overloads>
        /// <summary>
        /// Finds an entity with the given primary key. If no entity is found then <c>null</c> is returned.
        /// <note type="important">
        /// As every field of the entity will be returned, it is strongly recommended that <see cref="Find{TProjection}"/> be used instead to avoid unnecessary overhead.
        /// </note>
        /// </summary>
        /// <remarks>
        /// The <typeparamref name="TEntity"/> must have a member decorated with a <see cref="System.ComponentModel.DataAnnotations.KeyAttribute"/>.
        /// </remarks>
        /// <param name="id">Id of the entity to find.</param>
        /// <returns>The entity found, or <c>null</c>.</returns>
        TEntity Find(int? id);

        /// <summary>
        /// Finds an entity using the specified predicate, projecting it to <typeparamref name="TProjection"/>. If no entity is found then <c>null</c> is returned.
        /// <note type="note">
        /// Only the fields that actually get projected will be queried from the underlying data store, improving performance.
        /// </note>
        /// </summary>
        /// <typeparam name="TProjection">Type to project the entity to.</typeparam>
        /// <param name="predicate">Predicate used to find the entity.</param>
        /// <returns>The projected entity found, or <c>null</c>.</returns>
        TProjection Find<TProjection>(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// Adds an entity to the repository in such a manner that it will be inserted into the underling data store when <see cref="SaveChanges"/> is called.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        void Add(TEntity entity);

        /// <summary>
        /// Updates an entity in the repository in such a manner that the underlying data store will be updated when <see cref="SaveChanges"/> is called.
        /// </summary>
        /// <param name="entity">The entity to be updated.</param>
        void Update(TEntity entity);

        /// <summary>
        /// Marks an entity in the repository in such a manner that it will be deleted from the underling data store when <see cref="SaveChanges"/> is called.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        void Delete(TEntity entity);

        /// <summary>
        /// Saves all changes made to the repository to the underlying data store.
        /// </summary>
        /// <returns>The number of objects written to the underlying data store.</returns>
        int SaveChanges();
    }
}
