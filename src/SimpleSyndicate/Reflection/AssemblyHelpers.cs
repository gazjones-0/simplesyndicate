﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Reflection;

namespace SimpleSyndicate.Reflection
{
    /// <summary>
    /// Helper methods for working with <see cref="Assembly"/>s.
    /// </summary>
    public static class AssemblyHelpers
    {
        /// <summary>
        /// Gets the executing assembly version.
        /// </summary>
        /// <value>The executing assembly version.</value>
        public static Version ExecutingAssemblyVersion => Assembly.GetExecutingAssembly().GetName().Version;
    }
}
