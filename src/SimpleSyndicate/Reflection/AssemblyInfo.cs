﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using SimpleSyndicate.IO;

namespace SimpleSyndicate.Reflection
{
    /// <summary>
    /// Provides easy access to, and manipulation of, an <c>AssemblyInfo.cs</c> file.
    /// </summary>
    public class AssemblyInfo : StringFile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AssemblyInfo"/> class using the <c>AssemblyInfo.cs</c> at the specified <paramref name="path"/>.
        /// </summary>
        /// <param name="path">The <c>AssemblyInfo.cs</c> to use.</param>
        public AssemblyInfo(string path)
            : base(path)
        {
        }

        /// <summary>
        /// Gets or sets a value indicating whether the assembly is CLS compliant.
        /// </summary>
        /// <value><c>true</c> if the assembly is CLS compliant; <c>false</c> otherwise.</value>
        public bool CLSCompliant
        {
            get => GetAttributeValueAsBool("CLSCompliant");

            set => SetAttributeValue("CLSCompliant", value);
        }

        /// <summary>
        /// Gets or sets the assembly company.
        /// </summary>
        /// <value>The assembly company.</value>
        public string Company
        {
            get => GetAttributeValue("AssemblyCompany");

            set => SetAttributeValue("AssemblyCompany", value);
        }

        /// <summary>
        /// Gets or sets the assembly configuration.
        /// </summary>
        /// <value>The assembly configuration.</value>
        public string Configuration
        {
            get => GetAttributeValue("AssemblyConfiguration");

            set => SetAttributeValue("AssemblyConfiguration", value);
        }

        /// <summary>
        /// Gets or sets the assembly copyright.
        /// </summary>
        /// <value>The assembly copyright.</value>
        public string Copyright
        {
            get => GetAttributeValue("AssemblyCopyright");

            set => SetAttributeValue("AssemblyCopyright", value);
        }

        /// <summary>
        /// Gets or sets the assembly culture.
        /// </summary>
        /// <value>The assembly culture.</value>
        public string Culture
        {
            get => GetAttributeValue("AssemblyCulture");

            set => SetAttributeValue("AssemblyCulture", value);
        }

        /// <summary>
        /// Gets or sets the assembly description.
        /// </summary>
        /// <value>The assembly description.</value>
        public string Description
        {
            get => GetAttributeValue("AssemblyDescription");

            set => SetAttributeValue("AssemblyDescription", value);
        }

        /// <summary>
        /// Gets or sets the assembly file version.
        /// </summary>
        /// <value>The assembly file version.</value>
        public string FileVersion
        {
            get => GetAttributeValue("AssemblyFileVersion");

            set => SetAttributeValue("AssemblyFileVersion", value);
        }

        /// <summary>
        /// Gets or sets the assembly product.
        /// </summary>
        /// <value>The assembly product.</value>
        public string Product
        {
            get => GetAttributeValue("AssemblyProduct");

            set => SetAttributeValue("AssemblyProduct", value);
        }

        /// <summary>
        /// Gets or sets the assembly title.
        /// </summary>
        /// <value>The assembly title.</value>
        public string Title
        {
            get => GetAttributeValue("AssemblyTitle");

            set => SetAttributeValue("AssemblyTitle", value);
        }

        /// <summary>
        /// Gets or sets the assembly trademark.
        /// </summary>
        /// <value>The assembly trademark.</value>
        public string Trademark
        {
            get => GetAttributeValue("AssemblyTrademark");

            set => SetAttributeValue("AssemblyTrademark", value);
        }

        /// <summary>
        /// Gets or sets the assembly version.
        /// </summary>
        /// <value>The assembly version.</value>
        public string Version
        {
            get => GetAttributeValue("AssemblyVersion");

            set => SetAttributeValue("AssemblyVersion", value);
        }

        /// <summary>
        /// Returns the value of the attribute with the specified <paramref name="name"/>.
        /// </summary>
        /// <param name="name">The attribute name.</param>
        /// <returns>The attribute value.</returns>
        private string GetAttributeValue(string name)
        {
            var value = AttributeValue(name, false, string.Empty);
            if (string.IsNullOrWhiteSpace(value))
            {
                return value;
            }

            return value.Substring(1, value.Length - 2);
        }

        /// <summary>
        /// Returns the value of the attribute with the specified <paramref name="name"/> as a <see cref="bool"/>; if the
        /// value cannot be converted to a <see cref="bool"/>, <c>false</c> is returned.
        /// </summary>
        /// <param name="name">The attribute name.</param>
        /// <returns>The attribute value.</returns>
        private bool GetAttributeValueAsBool(string name)
        {
            if (bool.TryParse(AttributeValue(name, false, string.Empty), out var result))
            {
                return result;
            }

            return false;
        }

        /// <summary>
        /// Sets the value of the attribute with the specified <paramref name="name"/> to the specified <paramref name="newValue"/>.
        /// </summary>
        /// <param name="name">The attribute name.</param>
        /// <param name="newValue">The new attribute value.</param>
        private void SetAttributeValue(string name, string newValue)
        {
            AttributeValue(name, true, "\"" + (newValue ?? string.Empty) + "\"");
            Save();
        }

        /// <summary>
        /// Sets the value of the attribute with the specified <paramref name="name"/> to the specified <paramref name="newValue"/>.
        /// </summary>
        /// <param name="name">The attribute name.</param>
        /// <param name="newValue">The new attribute value.</param>
        private void SetAttributeValue(string name, bool newValue)
        {
            if (newValue == true)
            {
                AttributeValue(name, true, "true");
            }
            else
            {
                AttributeValue(name, true, "false");
            }

            Save();
        }

        /// <summary>
        /// Returns the value of the attribute with the specified <paramref name="name"/>; if <paramref name="replaceValue"/> is
        /// <c>true</c>, the original value is replaced with the specified <paramref name="newValue"/>.
        /// </summary>
        /// <param name="name">The attribute name.</param>
        /// <param name="replaceValue"><c>true</c> to replace the existing value with <paramref name="newValue"/>; <c>fales</c> otherwise.</param>
        /// <param name="newValue">The new attribute value.</param>
        /// <returns>The attribute value.</returns>
        private string AttributeValue(string name, bool replaceValue, string newValue)
        {
            // start at the beginning
            var startIndex = 0;

            // if the attribute isn't in the file, we'll add it in, but we'll try and put it in the appropriate place in the file,
            // which is after the first 'section'; if we can't find it, we'll default to this being the end of the file
            var firstSectionEnd = Contents.Length;
            while (startIndex >= 0)
            {
                // attribute?
                var assemblyIndex = Contents.IndexOf("[assembly", startIndex, StringComparison.OrdinalIgnoreCase);
                if (assemblyIndex >= 0)
                {
                    // yes, so find the start of the line the attribute is on by going backwards looking for a linebreak
                    var keepLooking = true;
                    var commentedOut = false;
                    var offset = 1;
                    while (keepLooking && Contents.Substring(assemblyIndex - offset, 1) != "\r" && Contents.Substring(assemblyIndex - offset, 1) != "\n")
                    {
                        // not found a linebreak, so check if the line has been commented out
                        if (Contents.Substring(assemblyIndex - offset, 2) == "//")
                        {
                            // it has been so no need to process further, and update startIndex so we look for the next attribute after this one
                            commentedOut = true;
                            keepLooking = false;
                            startIndex = assemblyIndex + 1;
                        }

                        // go back another character, but check we've not reached the start of the file (the attribute is the first line in this case)
                        offset++;
                        if ((assemblyIndex - offset) == 0)
                        {
                            keepLooking = false;
                        }
                    }

                    // not commented out?
                    if (commentedOut == false)
                    {
                        // no, so find the end of the assembly attribute
                        var colonIndex = Contents.IndexOf(":", assemblyIndex, StringComparison.OrdinalIgnoreCase);
                        if (colonIndex >= 0)
                        {
                            // find the end marker
                            var endIndex = Contents.IndexOf("]", colonIndex, StringComparison.OrdinalIgnoreCase);
                            if (endIndex >= 0)
                            {
                                // get everything after the colon, which should be the attribute
                                var assembly = Contents.Substring(colonIndex + 1, endIndex - colonIndex - 1).Trim();

                                // attribute we're looking for?
                                if (assembly.StartsWith(name + "(", StringComparison.OrdinalIgnoreCase) && assembly.EndsWith(")", StringComparison.OrdinalIgnoreCase))
                                {
                                    // yes, so work out where the value starts and ends
                                    var valueStart = Contents.IndexOf("(", colonIndex + 1, StringComparison.OrdinalIgnoreCase) + 1;
                                    var valueEnd = Contents.IndexOf(")", valueStart, StringComparison.OrdinalIgnoreCase);
                                    if (replaceValue == true)
                                    {
                                        // replace the existing value with the new value, and return the new value
                                        Contents =
                                            Contents.Substring(0, valueStart)
                                            + newValue
                                            + Contents.Substring(valueEnd);
                                        return newValue;
                                    }

                                    // not replacing the existing value, so just return it
                                    return Contents.Substring(valueStart, valueEnd - valueStart);
                                }
                                else
                                {
                                    // not the one we're looking for, but check to see if it's one of the ones that are normally in the
                                    // first 'section'; if it is, consider the end of this attribute the end of the first 'section'
                                    if (
                                        assembly.StartsWith("AssemblyTitle(", StringComparison.OrdinalIgnoreCase)
                                        || assembly.StartsWith("AssemblyDescription(", StringComparison.OrdinalIgnoreCase)
                                        || assembly.StartsWith("AssemblyConfiguration(", StringComparison.OrdinalIgnoreCase)
                                        || assembly.StartsWith("AssemblyCompany(", StringComparison.OrdinalIgnoreCase)
                                        || assembly.StartsWith("AssemblyProduct(", StringComparison.OrdinalIgnoreCase)
                                        || assembly.StartsWith("AssemblyCopyright(", StringComparison.OrdinalIgnoreCase)
                                        || assembly.StartsWith("AssemblyTrademark(", StringComparison.OrdinalIgnoreCase)
                                        || assembly.StartsWith("AssemblyCulture(", StringComparison.OrdinalIgnoreCase))
                                    {
                                        firstSectionEnd = endIndex + 1;
                                    }

                                    // look for another attribute after this one
                                    startIndex = endIndex + 1;
                                }
                            }
                            else
                            {
                                // no end marker (should never happen), just look for another attribute after this one
                                startIndex = colonIndex + 1;
                            }
                        }
                        else
                        {
                            // no colon (should never happen), just look for another attribute after this one
                            startIndex = assemblyIndex + 1;
                        }
                    }
                }
                else
                {
                    // no more attributes, so stop searching
                    startIndex = -1;
                }
            }

            // not found, add in special cases which might not normally exist, but only if we're replacing values
            if (replaceValue == true)
            {
                if (string.Compare(name, "CLSCompliant", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    firstSectionEnd = firstSectionEnd + 2;
                    Contents = Contents.Substring(0, firstSectionEnd)
                        + "\r\n"
                        + "// CLS compliance\r\n"
                        + "[assembly: CLSCompliant(" + newValue + ")]\r\n"
                        + Contents.Substring(firstSectionEnd);

                    // the CLSCompliant attribute is in the System namespace, and because it isn't normally in AssemblyInfo.cs,
                    // there's not normally a using System statement, so add one in if necessary
                    if (Contents.IndexOf("using System;", StringComparison.OrdinalIgnoreCase) == -1)
                    {
                        Contents = "using System;\r\n" + Contents;
                    }
                }
            }

            // if we can't find the attribute, just return an empty string
            return string.Empty;
        }
    }
}
