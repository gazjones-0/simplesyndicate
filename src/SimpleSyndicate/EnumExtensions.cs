﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.ComponentModel;
using System.Reflection;

namespace SimpleSyndicate
{
    /// <summary>
    /// The <see cref="EnumExtensions"/> class contains methods that extend the <see cref="Enum"/> class.
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// Returns the <see cref="Enum"/> description attribute.
        /// </summary>
        /// <param name="en">The <see cref="Enum"/> instance that this method extends.</param>
        /// <returns>The <see cref="Enum"/> description.</returns>
        public static string Description(this Enum en)
        {
            if (en == null)
            {
                throw new ArgumentNullException(nameof(en));
            }

            MemberInfo[] memInfo = en.GetType().GetMember(en.ToString());
            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }
    }
}
