﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Generic;

namespace SimpleSyndicate
{
    /// <summary>
    /// Helper methods for working with <see cref="Type"/>s.
    /// </summary>
    public static class BuiltInTypeHelpers
    {
        /// <summary>
        /// Built-in type aliases, keyed on <see cref="Type"/>.
        /// </summary>
        private static readonly Dictionary<Type, string> TypeAliases = new Dictionary<Type, string>()
        {
            // built-in types
            { typeof(byte), "byte" },
            { typeof(sbyte), "sbyte" },
            { typeof(short), "short" },
            { typeof(ushort), "ushort" },
            { typeof(int), "int" },
            { typeof(uint), "uint" },
            { typeof(long), "long" },
            { typeof(ulong), "ulong" },
            { typeof(float), "float" },
            { typeof(double), "double" },
            { typeof(decimal), "decimal" },
            { typeof(object), "object" },
            { typeof(bool), "bool" },
            { typeof(char), "char" },
            { typeof(string), "string" },
            { typeof(void), "void" },

            // nullable built-in types
            { typeof(byte?), "byte?" },
            { typeof(sbyte?), "sbyte?" },
            { typeof(short?), "short?" },
            { typeof(ushort?), "ushort?" },
            { typeof(int?), "int?" },
            { typeof(uint?), "uint?" },
            { typeof(long?), "long?" },
            { typeof(ulong?), "ulong?" },
            { typeof(float?), "float?" },
            { typeof(double?), "double?" },
            { typeof(decimal?), "decimal?" },
            { typeof(bool?), "bool?" },
            { typeof(char?), "char?" }
        };

        /// <summary>
        /// Built-in type aliases, keyed on the type name.
        /// </summary>
        private static readonly Dictionary<string, string> TypeNameAliases = new Dictionary<string, string>()
        {
            // non-qualified System types
            { "Byte", "byte" },
            { "SByte", "sbyte" },
            { "Int16", "short" },
            { "UInt16", "ushort" },
            { "Int32", "int" },
            { "UInt32", "uint" },
            { "Int64", "long" },
            { "UInt64", "ulong" },
            { "Single", "float" },
            { "Double", "double" },
            { "Decimal", "decimal" },
            { "Object", "object" },
            { "Boolean", "bool" },
            { "Char", "char" },
            { "String", "string" },

            // fully qualified System types
            { "System.Byte", "byte" },
            { "System.SByte", "sbyte" },
            { "System.Int16", "short" },
            { "System.UInt16", "ushort" },
            { "System.Int32", "int" },
            { "System.UInt32", "uint" },
            { "System.Int64", "long" },
            { "System.UInt64", "ulong" },
            { "System.Single", "float" },
            { "System.Double", "double" },
            { "System.Decimal", "decimal" },
            { "System.Object", "object" },
            { "System.Boolean", "bool" },
            { "System.Char", "char" },
            { "System.String", "string" },

            // non-qualified nullable built-in types
            { "Nullable<byte>", "byte?" },
            { "Nullable<sbyte>", "sbyte?" },
            { "Nullable<short>", "short?" },
            { "Nullable<ushort>", "ushort?" },
            { "Nullable<int>", "int?" },
            { "Nullable<uint>", "uint?" },
            { "Nullable<long>", "long?" },
            { "Nullable<ulong>", "ulong?" },
            { "Nullable<float>", "float?" },
            { "Nullable<double>", "double?" },
            { "Nullable<decimal>", "decimal?" },
            { "Nullable<bool>", "bool?" },
            { "Nullable<char>", "char?" },

            // non-qualified nullable non-qualified System types
            { "Nullable<Byte>", "byte?" },
            { "Nullable<SByte>", "sbyte?" },
            { "Nullable<Int16>", "short?" },
            { "Nullable<UInt16>", "ushort?" },
            { "Nullable<Int32>", "int?" },
            { "Nullable<UInt32>", "uint?" },
            { "Nullable<Int64>", "long?" },
            { "Nullable<UInt64>", "ulong?" },
            { "Nullable<Single>", "float?" },
            { "Nullable<Double>", "double?" },
            { "Nullable<Decimal>", "decimal?" },
            { "Nullable<Boolean>", "bool?" },
            { "Nullable<Char>", "char?" },

            // non-qualified nullable fully qualified System types
            { "Nullable<System.Byte>", "byte?" },
            { "Nullable<System.SByte>", "sbyte?" },
            { "Nullable<System.Int16>", "short?" },
            { "Nullable<System.UInt16>", "ushort?" },
            { "Nullable<System.Int32>", "int?" },
            { "Nullable<System.UInt32>", "uint?" },
            { "Nullable<System.Int64>", "long?" },
            { "Nullable<System.UInt64>", "ulong?" },
            { "Nullable<System.Single>", "float?" },
            { "Nullable<System.Double>", "double?" },
            { "Nullable<System.Decimal>", "decimal?" },
            { "Nullable<System.Boolean>", "bool?" },
            { "Nullable<System.Char>", "char?" },

            // fully qualified nullable built-in types
            { "System.Nullable<byte>", "byte?" },
            { "System.Nullable<sbyte>", "sbyte?" },
            { "System.Nullable<short>", "short?" },
            { "System.Nullable<ushort>", "ushort?" },
            { "System.Nullable<int>", "int?" },
            { "System.Nullable<uint>", "uint?" },
            { "System.Nullable<long>", "long?" },
            { "System.Nullable<ulong>", "ulong?" },
            { "System.Nullable<float>", "float?" },
            { "System.Nullable<double>", "double?" },
            { "System.Nullable<decimal>", "decimal?" },
            { "System.Nullable<bool>", "bool?" },
            { "System.Nullable<char>", "char?" },

            // fully qualified nullable non-qualified System types
            { "System.Nullable<Byte>", "byte?" },
            { "System.Nullable<SByte>", "sbyte?" },
            { "System.Nullable<Int16>", "short?" },
            { "System.Nullable<UInt16>", "ushort?" },
            { "System.Nullable<Int32>", "int?" },
            { "System.Nullable<UInt32>", "uint?" },
            { "System.Nullable<Int64>", "long?" },
            { "System.Nullable<UInt64>", "ulong?" },
            { "System.Nullable<Single>", "float?" },
            { "System.Nullable<Double>", "double?" },
            { "System.Nullable<Decimal>", "decimal?" },
            { "System.Nullable<Boolean>", "bool?" },
            { "System.Nullable<Char>", "char?" },

            // fully qualified nullable fully qualified System types
            { "System.Nullable<System.Byte>", "byte?" },
            { "System.Nullable<System.SByte>", "sbyte?" },
            { "System.Nullable<System.Int16>", "short?" },
            { "System.Nullable<System.UInt16>", "ushort?" },
            { "System.Nullable<System.Int32>", "int?" },
            { "System.Nullable<System.UInt32>", "uint?" },
            { "System.Nullable<System.Int64>", "long?" },
            { "System.Nullable<System.UInt64>", "ulong?" },
            { "System.Nullable<System.Single>", "float?" },
            { "System.Nullable<System.Double>", "double?" },
            { "System.Nullable<System.Decimal>", "decimal?" },
            { "System.Nullable<System.Boolean>", "bool?" },
            { "System.Nullable<System.Char>", "char?" },
        };

        /// <summary>
        /// Returns the equivalent built-in type name for the specified <paramref name="name"/> if it has one, or the <paramref name="name"/> if
        /// it doesn't; e.g. <c>String</c> would be returned as <c>string</c>, or <c>Boolean</c> would be returned as <c>bool</c>.
        /// </summary>
        /// <param name="name">The name to find the built-in equivalent for.</param>
        /// <returns>The equivalent built-in type name, or the <paramref name="name"/> if there's no equivalent.</returns>
        public static string BuiltInTypeName(string name)
        {
            if (TypeNameAliases.TryGetValue(name, out string builtInName))
            {
                return builtInName;
            }

            return name;
        }

        /// <summary>
        /// Returns the equivalent built-in type name for the specified <paramref name="type"/> if it has one, or the type's name if it doesn't; e.g. a
        /// <c>System.String</c> type would return <c>string</c>, or a <c>System.Boolean</c> type would return <c>bool</c>.
        /// </summary>
        /// <param name="type">The <see cref="Type"/> to find the built-in equivalent for.</param>
        /// <returns>The equivalent built-in type name, or the <paramref name="type"/>'s name if there's no equivalent.</returns>
        public static string BuiltInTypeName(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            if (TypeAliases.TryGetValue(type, out string name))
            {
                return name;
            }

            return type.Name;
        }
    }
}
