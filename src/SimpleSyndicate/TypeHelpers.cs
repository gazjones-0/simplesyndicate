﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;

namespace SimpleSyndicate
{
    /// <summary>
    /// Helper methods for working with <see cref="Type"/>s.
    /// </summary>
    public static class TypeHelpers
    {
        /// <summary>
        /// Returns the friendly name of the specified <paramref name="type"/>; for non-generic types this is just the name (e.g. String), whilst for generic types
        /// this is the name plus the type arguments (e.g. IList&lt;String&gt;).
        /// </summary>
        /// <param name="type">The <see cref="Type"/> to get the friendly name of.</param>
        /// <returns>The <paramref name="type"/>'s friendly name.</returns>
        public static string FriendlyName(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var friendlyName = type.Name;
            if (type.IsGenericType)
            {
                var backtick = friendlyName.IndexOf("`", StringComparison.OrdinalIgnoreCase);
                if (backtick > 0)
                {
                    friendlyName = friendlyName.Substring(0, backtick);
                }

                friendlyName = friendlyName + "<";
                var parameters = type.GetGenericArguments();
                friendlyName = friendlyName + parameters[0].Name;
                for (int index = 1; index < parameters.Length; index++)
                {
                    friendlyName = friendlyName + ", " + parameters[index].Name;
                }

                friendlyName = friendlyName + ">";
            }

            return friendlyName;
        }

        /// <summary>
        /// Returns the full friendly name of the specified <paramref name="type"/>; for non-generic types this is the full name (e.g. System.String), whilst
        /// for generic types this is the full name plus the full type arguments (e.g. System.Collections.Generic.IList&lt;System.String&gt;).
        /// </summary>
        /// <param name="type">The <see cref="Type"/> to get the full friendly name of.</param>
        /// <returns>The <paramref name="type"/>'s full friendly name.</returns>
        public static string FullFriendlyName(this Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            var friendlyName = type.FullName;
            if (type.IsGenericType)
            {
                var backtick = friendlyName.IndexOf("`", StringComparison.OrdinalIgnoreCase);
                if (backtick > 0)
                {
                    friendlyName = friendlyName.Substring(0, backtick);
                }

                friendlyName = friendlyName + "<";
                var parameters = type.GetGenericArguments();
                friendlyName = friendlyName + parameters[0].FullName;
                for (int index = 1; index < parameters.Length; index++)
                {
                    friendlyName = friendlyName + ", " + parameters[index].FullName;
                }

                friendlyName = friendlyName + ">";
            }

            return friendlyName;
        }

        /// <summary>
        /// Gets the name of the specified <paramref name="type"/> using <see cref="System.CodeDom"/>, which will provide a more readable name.
        /// </summary>
        /// <param name="type">The <see cref="Type"/> to get the name of.</param>
        /// <returns>The <paramref name="type"/>'s name.</returns>
        /// <exception cref="ArgumentNullException">Thrown when <paramref name="type"/> is <c>null</c>.</exception>
        public static string FullNameFromCodeDom(Type type)
        {
            if (type == null)
            {
                throw new ArgumentNullException(nameof(type));
            }

            using (var provider = System.CodeDom.Compiler.CodeDomProvider.CreateProvider("CSharp"))
            {
                return provider.GetTypeOutput(new System.CodeDom.CodeTypeReference(type.FullName));
            }
        }
    }
}
