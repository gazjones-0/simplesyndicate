﻿// Copyright (c) simpleSyndicate. All rights reserved.
// Licensed under the MIT license. See LICENSE.txt file in the project root for full license information.

using System;
using System.Collections.Specialized;

namespace SimpleSyndicate
{
    /// <summary>
    /// Helper methods for working with <see cref="Uri"/>s.
    /// </summary>
    public static class UriHelpers
    {
        /// <summary>
        /// Builds a URI from a base URI and a path.
        /// </summary>
        /// <param name="baseAddress">Base URI.</param>
        /// <param name="path">Path to resource.</param>
        /// <returns>A <see cref="Uri"/> object formed from the <paramref name="baseAddress"/> and the <paramref name="path"/>.</returns>
        public static Uri BuildUri(string baseAddress, string path)
        {
            var parameters = new NameValueCollection();
            return BuildUri(baseAddress, path, parameters);
        }

        /// <summary>
        /// Builds a URI from a base URI, path and a single parameter.
        /// </summary>
        /// <param name="baseAddress">Base URI.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName">Name of the parameter.</param>
        /// <param name="parameterValue">Value of the parameter.</param>
        /// <returns>A <see cref="Uri"/> object formed from the <paramref name="baseAddress"/>, the <paramref name="path"/> and the parameter.</returns>
        public static Uri BuildUri(string baseAddress, string path, string parameterName, string parameterValue)
        {
            var parameters = new NameValueCollection()
            {
                { parameterName, parameterValue }
            };
            return BuildUri(baseAddress, path, parameters);
        }

        /// <summary>
        /// Builds a URI from a base URI, path and two parameters.
        /// </summary>
        /// <param name="baseAddress">Base URI.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <returns>A <see cref="Uri"/> object formed from the <paramref name="baseAddress"/>, the <paramref name="path"/> and the two parameters.</returns>
        public static Uri BuildUri(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2)
        {
            var uriParameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 }
            };
            return BuildUri(baseAddress, path, uriParameters);
        }

        /// <summary>
        /// Builds a URI from a base URI, path and three parameters.
        /// </summary>
        /// <param name="baseAddress">Base URI.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="parameterName1">Name of the first parameter.</param>
        /// <param name="parameterValue1">Value of the first parameter.</param>
        /// <param name="parameterName2">Name of the second parameter.</param>
        /// <param name="parameterValue2">Value of the second parameter.</param>
        /// <param name="parameterName3">Name of the third parameter.</param>
        /// <param name="parameterValue3">Value of the third parameter.</param>
        /// <returns>A <see cref="Uri"/> object formed from the <paramref name="baseAddress"/>, the <paramref name="path"/> and the three parameters.</returns>
        public static Uri BuildUri(string baseAddress, string path, string parameterName1, string parameterValue1, string parameterName2, string parameterValue2, string parameterName3, string parameterValue3)
        {
            var uriParameters = new NameValueCollection
            {
                { parameterName1, parameterValue1 },
                { parameterName2, parameterValue2 },
                { parameterName3, parameterValue3 }
            };
            return BuildUri(baseAddress, path, uriParameters);
        }

        /// <summary>
        /// Builds a URI from a base URI, path and a parameter collection.
        /// </summary>
        /// <param name="baseAddress">Base URI.</param>
        /// <param name="path">Path to resource.</param>
        /// <param name="uriParameters">Collection of parameters.</param>
        /// <returns>A <see cref="Uri"/> object formed from the <paramref name="baseAddress"/>, the <paramref name="path"/> and the <paramref name="uriParameters"/>.</returns>
        public static Uri BuildUri(string baseAddress, string path, NameValueCollection uriParameters)
        {
            var template = new UriTemplate(path);
            return template.BindByName(new Uri(baseAddress), uriParameters);
        }
    }
}
